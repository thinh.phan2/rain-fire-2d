import express from "express"
import cors from "cors"
import http from "http"
import { Server } from "socket.io";
import { SocketManager } from "./Socket/Manager/SocketManager";

const app = express();
const server = http.createServer(app);

app.use(cors());

const io: Server = require("socket.io")(server, {
    cors: {
        origin: "*",
    },
});

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

new SocketManager(io);

io.on("connection", (socket) => {
    SocketManager.instance.socketFunction(io, socket);
});

server.listen(3000, () => {
    console.log("listening on *:3000");
});