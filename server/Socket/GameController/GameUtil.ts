export class GameUtil {
    public static removeByID(array, id) {
        if (array == null) return;
        let i = array.indexOf(id)
        if (i !== -1) {
            array.splice(i, 1);
        }
    }

    public static addByID(array, id) {
        if (array.indexOf(id) === -1) {
            array.push(id);
        }
    }

    public static checkIntersectTwoRectangle(x1, y1, w1, h1, x2, y2, w2, h2) {
        x1 = x1 - w1 / 2;
        x2 = x2 - w2 / 2;
        y1 = y1 - h1 / 2;
        y2 = y2 - h2 / 2;
        return ((x1 + w1 >= x2) && (x2 + w2 >= x1) && (y1 + h1 >= y2) && (y2 + h2 >= y1));
    }

    public static clamp(value, min, max) {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }

    public static randomRangeInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }
}

