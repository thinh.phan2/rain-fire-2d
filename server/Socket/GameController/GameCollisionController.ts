import { GameUtil } from './GameUtil';
import { Game } from "../Component/Game";
import { GameDefine } from "../Defines";
import { GameManager } from "../Manager/GameManager";

export class GameCollisionController {

    private _gameID: string;

    constructor(gameID) {
        this._gameID = gameID;
    }

    public get game(): Game {
        return GameManager.instance.getGame(this._gameID);
    }

    public checkCollision() {
        //Check collision player
        for (let [playerID, player] of this.game.players) {
            if (!player || player.isDie) continue;

            let xCeil = Math.ceil(player.x);
            let xFloor = Math.floor(player.x);
            let yCeil = Math.ceil(player.y);
            let yFloor = Math.floor(player.y);

            let w = 1;
            let h = 1;

            //kiểm tra va chạm với đạn
            this.checkPlayerCollisionOnBullet(player.x, player.y, w, h, xCeil, yCeil, playerID);
            this.checkPlayerCollisionOnBullet(player.x, player.y, w, h, xCeil, yFloor, playerID);
            this.checkPlayerCollisionOnBullet(player.x, player.y, w, h, xFloor, yCeil, playerID);
            this.checkPlayerCollisionOnBullet(player.x, player.y, w, h, xFloor, yFloor, playerID);

            //kiểm tra va chạm với item
            this.checkPlayerCollisionOnItem(player.x, player.y, w, h, xCeil, yCeil, playerID);
            this.checkPlayerCollisionOnItem(player.x, player.y, w, h, xCeil, yFloor, playerID);
            this.checkPlayerCollisionOnItem(player.x, player.y, w, h, xFloor, yCeil, playerID);
            this.checkPlayerCollisionOnItem(player.x, player.y, w, h, xFloor, yFloor, playerID);

            if (!this.checkPlayerCollisionOnTeleport(player.x, player.y, w, h, xCeil, yCeil, playerID)
                && !this.checkPlayerCollisionOnTeleport(player.x, player.y, w, h, xCeil, yFloor, playerID)
                && !this.checkPlayerCollisionOnTeleport(player.x, player.y, w, h, xFloor, yCeil, playerID)
                && !this.checkPlayerCollisionOnTeleport(player.x, player.y, w, h, xFloor, yFloor, playerID)) {
                if (player.readyTeleport == false) {
                    player.readyTeleport = true;
                }
            }
        }


        //Check collision bullet vs tile
        for (let [bulletID, bullet] of this.game.bullets) {
            if (!bullet) continue;

            let xCeil = Math.ceil(bullet.x);
            let xFloor = Math.floor(bullet.x);
            let yCeil = Math.ceil(bullet.y);
            let yFloor = Math.floor(bullet.y);

            //size bullet
            let w = bullet.w;
            let h = bullet.h;

            //kiểm tra va chạm của hình chữ nhật vào bởi tia đạn
            let xCheck = bullet.x;
            let yCheck = (bullet.y + bullet.yPre) / 2;
            w = Math.abs(bullet.y - bullet.yPre) + w;

            this.checkBulletCollisionOnTile(xCheck, yCheck, w, h, xCeil, yCeil, bulletID)
            this.checkBulletCollisionOnTile(xCheck, yCheck, w, h, xCeil, yFloor, bulletID)
            this.checkBulletCollisionOnTile(xCheck, yCheck, w, h, xFloor, yCeil, bulletID)
            this.checkBulletCollisionOnTile(xCheck, yCheck, w, h, xFloor, yFloor, bulletID);
        }
    }

    public checkBulletCollisionOnTile(x, y, w, h, xTile, yTile, bulletID) {
        if (this.game.mapData[xTile][yTile].tile == 0) return false;

        let bullet = this.game.bullets.get(bulletID);
        if (!bullet) return false;

        //check collision vs bullet
        let bulletIDs = this.game.mapData[xTile][yTile].bulletIDs;

        let n = bulletIDs.length;
        if (n > 0)
            for (let i = 0; i < n; i++) {
                let bulletOnTileID = bulletIDs[i];
                let bulletOnTile = this.game.bullets.get(bulletOnTileID);
                if (!bulletOnTile) continue;

                //size bullet
                let w = bullet.w;
                let h = bullet.h;

                //kiểm tra va chạm của hình chữ nhật vào bởi tia đạn
                let yCheck = (bullet.y + bullet.yPre) / 2;
                w = Math.abs(bullet.y - bullet.yPre) + w;

                if (bulletOnTile.playerID !== bullet.playerID) {
                    if (GameUtil.checkIntersectTwoRectangle(y, x, w, h, yCheck, bullet.x, w, h)) {
                        if (bullet.damage == bulletOnTile.damage) {
                            this.game.gameBulletController.destroyBullet(bulletOnTileID);
                            this.game.gameBulletController.destroyBullet(bulletID);
                        } else if (bullet.damage < bulletOnTile.damage) {
                            this.game.gameBulletController.destroyBullet(bulletID);
                        }else {
                            this.game.gameBulletController.destroyBullet(bulletOnTileID);
                        }

                        return true;
                    }
                }
            }

        //check collision vs tile
        if (GameUtil.checkIntersectTwoRectangle(y, x, w, h, yTile, xTile, GameDefine.unit, GameDefine.unit)) {
            this.game.gameBulletController.destroyBullet(bulletID);
            return true;
        }
    }

    public checkPlayerCollisionOnBullet(x, y, w, h, xTile, yTile, playerID) {
        let bulletIDs = this.game.mapData[xTile][yTile].bulletIDs;

        let n = bulletIDs.length;
        if (n > 0)
            for (let i = 0; i < n; i++) {
                let bulletID = bulletIDs[i];
                let bullet = this.game.bullets.get(bulletID);
                if (!bullet) continue;

                if (bullet.playerID !== playerID) {
                    let yCheck = (bullet.y + bullet.yPre) / 2;
                    w = Math.abs(bullet.y - bullet.yPre) + w;

                    if (GameUtil.checkIntersectTwoRectangle(y, x, w, h, yCheck, bullet.x, w, bullet.h)) {
                        this.game.gamePlayerController.takenDamage(playerID, bullet.damage, bullet.playerID);
                        this.game.gameBulletController.destroyBullet(bulletID);
                        return true;
                    }
                }
            }
    }

    public checkPlayerCollisionOnItem(x, y, w, h, xTile, yTile, playerID) {
        //check player collision
        let itemIDs = this.game.mapData[xTile][yTile].itemIDs;

        let n = itemIDs.length;
        if (n > 0)
            for (let i = 0; i < n; i++) {
                let itemID = itemIDs[i];
                let item = this.game.items.get(itemID);
                if (!item || !item.isEnable) continue;

                if (GameUtil.checkIntersectTwoRectangle(y, x, w, h, item.y, item.x, 1, 1)) {
                    this.game.gamePlayerController.claimItem(playerID, item.type);
                    this.game.gameItemController.destroyItem(itemID, playerID);
                    return true;
                }
            }
    }

    public checkPlayerCollisionOnTeleport(x, y, w, h, xTile, yTile, playerID) {
        //check player collision
        let teleportIDs = this.game.mapData[xTile][yTile].teleportIDs;

        let n = teleportIDs.length;
        if (n > 0)
            for (let i = 0; i < n; i++) {
                let teleportID = teleportIDs[i];
                let teleport = this.game.teleports.get(teleportID);
                if (!teleport) continue;

                if (GameUtil.checkIntersectTwoRectangle(y, x, w, h, teleport.position.y, teleport.position.x, 1, 1)) {
                    this.game.gamePlayerController.enterTeleport(playerID, teleport);
                    return true;
                }
            }

        return false;
    }
}

