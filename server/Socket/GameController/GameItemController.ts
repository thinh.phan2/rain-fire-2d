import { GameDefine } from './../Defines';
import { GameManager } from './../Manager/GameManager';
import { Game } from "../Component/Game";
import { MockSocketMessage } from '../MockSocketMessage';
import { SocketMessageTitle } from '../Defines';
import { item } from '../Interfaces';
import { GameUtil } from './GameUtil';

export class GameItemController {

    private _gameID: string;

    constructor(gameID) {
        this._gameID = gameID;
    }

    public get game(): Game {
        return GameManager.instance.getGame(this._gameID);
    }

    public destroyItem(itemID: string, playerID: string) {
        let item = this.game.items.get(itemID);
        if (!item) return;

        item.isEnable = false;

        setTimeout(() => {
            this.respawn(item);
        }, GameDefine.itemRespawnTime);

        let result: typeof MockSocketMessage.ON_PLAYER_CLAIM_ITEM = {
            title: SocketMessageTitle.PlayerClaimItem,
            data: {
                itemID,
                playerID
            }
        }

        this.game.sendMessageToGame(result);
    }

    public deleteItemMapData(item: item) {
        let mapData = this.game.mapData;
        //xoa cac vi tri truoc do
        GameUtil.removeByID(mapData[Math.ceil(item.x)][Math.ceil(item.y)].itemIDs, item.itemID);
        GameUtil.removeByID(mapData[Math.ceil(item.x)][Math.floor(item.y)].itemIDs, item.itemID);
    }

    public respawn(item: item) {
        if(!this.game) return;

        item.isEnable = true;
        let result: typeof MockSocketMessage.ON_ITEM_RESPAWN = {
            title: SocketMessageTitle.ItemRespawn,
            data: {
                ...item
            }
        }

        this.game.sendMessageToGame(result);
    }
}

