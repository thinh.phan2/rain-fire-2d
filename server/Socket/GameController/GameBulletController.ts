import { GameManager } from '../Manager/GameManager';
import { Game } from "../Component/Game";
import { GameDefine, SocketMessageTitle, WeaponType } from '../Defines';
import { GameUtil } from './GameUtil';
import { MockSocketMessage } from '../MockSocketMessage';
import { Utils } from '../../Utils';
import { bullet } from '../Interfaces';

export class GameBulletController {

    private _gameID: string;

    constructor(gameID) {
        this._gameID = gameID;
    }

    public get game(): Game {
        return GameManager.instance.getGame(this._gameID);
    }

    public updateBulletNextStatus() {
        let currentTime = this.game.currentTime;

        for (let [bulletID, bullet] of this.game.bullets) {
            if (!bullet) return;

            this.removePreBulletMapData(bullet);

            //lưu giá trị vị trí trước đó
            bullet.yPre = bullet.y;

            //cập nhập vị trí cho viên đạn
            let timeDuration = currentTime - bullet.timeBegin;
            bullet.y = bullet.yBegin + (timeDuration * bullet.speed * GameDefine._1divideTimePerFrame) * bullet.direction;
            bullet.y = Number(bullet.y.toFixed(GameDefine.fixedFloat));
            bullet.y = GameUtil.clamp(bullet.y, 0, GameDefine.width - 1);

            if ((bullet.direction == 1 && bullet.y >= bullet.yDestination) || (bullet.direction == -1 && bullet.y <= bullet.yDestination)) {
                bullet.isDestroy = true;
                bullet.y = bullet.yDestination;
            }

            this.setBulletMapData(bullet);

            if (bullet.isDestroy) {
                this.destroyBullet(bulletID);
                return;
            }
        }
    }

    public playerShot(data: typeof MockSocketMessage.SEND_PLAYER_SHOT.data) {
        let playerID = data.playerID;

        let players = this.game.players;
        let player = players.get(playerID);
        if (!player || player.isDie) return;

        let damage = 1;
        let speed = GameDefine.bulletSpeed;

        switch (player.gunType) {
            case WeaponType.FireGun:
            case WeaponType.Rocket:
                damage = 2;
                break;
        }

        //hard code follow firepoint on client
        let firePointX = 35 / 40;
        let firePointY = -3 / 40;

        if (player.fireTime >= player.fireRate) {
            player.fireTime = 0;

            let x = player.x;
            let y = player.y;

            x += firePointY;
            y += firePointX * player.direction;

            y = Number(y.toFixed(GameDefine.fixedFloat));
            x = Number(x.toFixed(GameDefine.fixedFloat));

            let bulletID = Utils.uuid;
            let distance = GameDefine.buttetDistance;
            let yDestination = y + distance * player.direction;
            yDestination = GameUtil.clamp(yDestination, 0, GameDefine.width - 1);


            //init bullet
            let bullet: bullet = {
                bulletID,
                playerID,
                x,
                y,
                yPre: y,
                yBegin: y,
                w: 1 / 8,
                h: 1 / 8,
                yDestination,
                direction: player.direction,
                isDestroy: false,
                speed, // quang duong di duoc moi frame => timeDuration =  (distance / speed) * timeperframe
                timeBegin: Date.now(),
                damage
            }
            this.game.bullets.set(bulletID, bullet);

            this.sendBulletMessage(bullet);
        }
    }

    public destroyBullet(bulletID: string) {
        let bullet = this.game.bullets.get(bulletID);
        if (!bullet) return;
        bullet.isDestroy = true;

        this.removePreBulletMapData(bullet);
        this.game.bullets.delete(bulletID);

        this.sendBulletMessage(bullet);
    }

    private setBulletMapData(bullet: bullet) {
        // console.log(bullet.yPre, bullet.y)
        let mapData = this.game.mapData;
        let yBegin;
        let yEnd;

        if (bullet.direction == 1) {
            yBegin = Math.floor(bullet.yPre);
            yEnd = Math.ceil(bullet.y);
        } else {
            yEnd = Math.ceil(bullet.yPre);
            yBegin = Math.floor(bullet.y);
        }

        for (let y = yBegin; y <= yEnd; y++) {
            GameUtil.addByID(mapData[Math.ceil(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.addByID(mapData[Math.ceil(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.addByID(mapData[Math.floor(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.addByID(mapData[Math.floor(bullet.x)][y].bulletIDs, bullet.bulletID);
        }

        // mapData[3].forEach((element, index) => {
        //     if(element.bulletIDs.length > 0)
        //     {
        //         console.log(index);
        //     }
        // })
    }

    private removePreBulletMapData(bullet: bullet) {
        let mapData = this.game.mapData;
        let yBegin;
        let yEnd;

        if (bullet.direction == 1) {
            yBegin = Math.floor(bullet.yPre);
            yEnd = Math.ceil(bullet.y);
        } else {
            yEnd = Math.ceil(bullet.yPre);
            yBegin = Math.floor(bullet.y);
        }

        for (let y = yBegin; y <= yEnd; y++) {
            GameUtil.removeByID(mapData[Math.ceil(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.removeByID(mapData[Math.ceil(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.removeByID(mapData[Math.floor(bullet.x)][y].bulletIDs, bullet.bulletID);
            GameUtil.removeByID(mapData[Math.floor(bullet.x)][y].bulletIDs, bullet.bulletID);
        }
    }


    private sendBulletMessage(bullet: bullet) {
        let message: typeof MockSocketMessage.ON_PLAYER_SHOT = {
            title: SocketMessageTitle.PlayerShot,
            data: {
                bulletID: bullet.bulletID,
                playerID: bullet.playerID,
                x: bullet.x,
                y: bullet.y,
                yDestination: bullet.yDestination,
                speed: bullet.speed,
                direction: bullet.direction,
                isDestroy: bullet.isDestroy
            }
        }

        this.game.sendMessageToGame(message);
    }
}

