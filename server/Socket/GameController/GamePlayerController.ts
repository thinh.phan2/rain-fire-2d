import { GameManager } from '../Manager/GameManager';
import { Game } from "../Component/Game";
import { GameDefine, ItemType, PlayerUpdateType, SocketMessageTitle, WeaponType } from '../Defines';
import { GameUtil } from './GameUtil';
import { MockSocketMessage } from '../MockSocketMessage';
import { player, teleport } from '../Interfaces';
import { MockData } from '../MockData';

export class GamePlayerController {
    private _gameID: string;

    constructor(gameID) {
        this._gameID = gameID;
    }

    public get game(): Game {
        return GameManager.instance.getGame(this._gameID);
    }

    public updatePlayerNextStatus() {
        for (let [playerID, player] of this.game.players) {
            player.fireTime += GameDefine.timePerFrame;
            this.handlePlayerDrop(playerID);
            this.checkPlayerState(playerID);

            if (!player || player.isDie) continue;

            if (player.moveMessage) {
                this.game.handleMessage(player.moveMessage)
                player.moveMessage = null;
            }

            if (player.jumpMessage) {
                this.game.handleMessage(player.jumpMessage)
                player.jumpMessage = null;
            }

            if (player.shotMessage) {
                this.game.handleMessage(player.shotMessage)
                player.shotMessage = null;
            }

            let x = player.x;
            let y = player.y;

            let mapData = this.game.mapData;

            if (player.isJumping) {
                x = x + player.jumpSpeed;
                x = Number(x.toFixed(GameDefine.fixedFloat));

                let xCeil = Math.ceil(x);
                let xFloor = Math.floor(x);

                if (xFloor == player.xJumping) { // nếu đã tới vị trí cần -> kết thúc việc gửi cho client

                    this.move(xFloor, y, playerID);
                    player.isJumping = false;

                } else {
                    if (mapData[xCeil][Math.floor(y)].tile != 0 || mapData[xCeil][Math.ceil(y)].tile != 0) { //Nếu phía trên player có ô
                        x = xCeil - 1;
                        player.isJumping = false;
                    }
                    this.move(x, y, playerID);
                }
            } else if (player.isDroping) {
                player.dropSpeed += 1 / 60; // gia toc roi
                x = x - player.dropSpeed;
                x = Number(x.toFixed(GameDefine.fixedFloat));

                let xFloor = Math.floor(x);
                let xCeil = Math.ceil(x);

                //dừng rơi khi phía dưới player có ô
                if (mapData[xFloor][Math.floor(y)].tile != 0 || mapData[xFloor][Math.ceil(y)].tile != 0) {
                    this.move(xCeil, y, playerID);
                    player.isDroping = false;
                    player.dropSpeed = GameDefine.playerDropSpeed;
                } else {
                    this.move(x, y, playerID);
                }
            }
        }
    }

    public handlePlayerDrop(playerID) {
        let player = this.game.players.get(playerID);
        if (!player || player.isDie || player.isImmove) return;
        if (player.isJumping || player.isDroping) return;

        let x = player.x;
        let y = player.y;

        if (this.game.mapData[x - 1][Math.floor(y)].tile == 0 && this.game.mapData[x - 1][Math.ceil(y)].tile == 0) // nếu phía dưới có ô trống => cho player rơi
        {
            player.isDroping = true;
        }
    }

    public checkPlayerState(playerID) {
        let player = this.game.players.get(playerID);
        if (!player || player.isDie) return;

        if (player.speedBuffEndTime != 0 && player.speedBuffEndTime <= Date.now()) {
            player.speedBuffEndTime = 0;
            player.speed = GameDefine.playerSpeed;

            let message = {
                title: SocketMessageTitle.PlayerUpdateStatus,
                data: {
                    playerID: player.playerID,
                    updateType: PlayerUpdateType.Speed,
                    enable: false,
                }
            }

            this.game.sendMessageToGame(message);
        }

        if (player.jumpBuffEndTime != 0 && player.jumpBuffEndTime <= Date.now()) {
            player.jumpBuffEndTime = 0;
            player.jumpSpeed = GameDefine.playerJumpSpeed;

            let message = {
                title: SocketMessageTitle.PlayerUpdateStatus,
                data: {
                    playerID: player.playerID,
                    updateType: PlayerUpdateType.Jump,
                    enable: false,
                }
            }

            this.game.sendMessageToGame(message);
        }

        if (player.gunBuffEndTime != 0 && player.gunBuffEndTime <= Date.now()) {
            player.gunBuffEndTime = 0;
            this.setGun(player, WeaponType.Pistol);
        }

        if (player.immoveEndTime != 0 && player.immoveEndTime <= Date.now()) {
            player.immoveEndTime = 0;
            player.isImmove = false;
        }

        if (player.invincibleEndTime != 0 && player.invincibleEndTime <= Date.now()) {
            player.invincibleEndTime = 0;
            player.isInvincible = false;
        }
    }

    public playerMove(data: typeof MockSocketMessage.SEND_PLAYER_MOVE.data) {
        let player = this.game.players.get(data.playerID);
        if (!player || player.isDie || player.isImmove) return;

        let direction = data.direction;
        let x = player.x;
        let y = player.y;
        player.direction = direction;

        if (!data.isJump) {
            y += player.speed * direction;
            y = Number(y.toFixed(GameDefine.fixedFloat));
            y = GameUtil.clamp(y, 0, GameDefine.width - 1);

            let yCheck = (direction == 1) ? Math.ceil(y) : Math.floor(y);
            let yResult = (direction == 1) ? (yCheck - 1) : (yCheck + 1);

            if (!player.isDroping && !player.isJumping) { // nếu player ko rơi or jump => chỉ check ở độ cao x
                if (this.game.mapData[x][yCheck].tile == 0) {
                    this.move(x, y, player.playerID);
                } else {
                    this.move(x, yResult, player.playerID);
                }
            } else { // nếu player đang rơi or jump => check ở cả 2 ô xung quanh độ cao x
                if (this.game.mapData[Math.ceil(x)][yCheck].tile == 0 && this.game.mapData[Math.floor(x)][yCheck].tile == 0) {
                    this.move(x, y, player.playerID);
                } else {
                    this.move(x, yResult, player.playerID);
                }
            }

        } else {
            if (player.isDroping || player.isJumping) return; // Không thể nhảy lúc rơi hoặc lúc nhảy

            player.isJumping = true;
            player.xJumping = player.x + player.jumpHigh;;
        }
    }

    public setPlayerMapData(x, y, playerID) {
        let player = this.game.players.get(playerID);
        if (!player || player.isDie) return;

        let xOld = player.x;
        let yOld = player.y;

        player.x = x;
        player.y = y;

        let mapData = this.game.mapData;

        //remove
        GameUtil.removeByID(mapData[Math.ceil(xOld)][Math.ceil(yOld)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.ceil(xOld)][Math.floor(yOld)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.floor(xOld)][Math.ceil(yOld)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.floor(xOld)][Math.floor(yOld)].playerIDs, playerID);

        //add
        GameUtil.addByID(mapData[Math.ceil(x)][Math.ceil(y)].playerIDs, playerID);
        GameUtil.addByID(mapData[Math.ceil(x)][Math.floor(y)].playerIDs, playerID);
        GameUtil.addByID(mapData[Math.floor(x)][Math.ceil(y)].playerIDs, playerID);
        GameUtil.addByID(mapData[Math.floor(x)][Math.floor(y)].playerIDs, playerID);
    }

    public claimItem(playerID, itemType: ItemType) {
        let players = this.game.players;
        let player = players.get(playerID);
        if (!player || player.isDie) return;

        let message;

        switch (itemType) {
            case ItemType.Health:
                this.changeHealth(player, 1);
                break;
            case ItemType.Shield:
                this.changeShield(player, 1);
                break;
            case ItemType.Speed:
                player.speed += player.speed * 0.3;
                player.speedBuffEndTime = Date.now() + GameDefine.speedBuffTime;

                message = {
                    title: SocketMessageTitle.PlayerUpdateStatus,
                    data: {
                        playerID: player.playerID,
                        updateType: PlayerUpdateType.Speed,
                        enable: true,
                    }
                }

                this.game.sendMessageToGame(message);

                break;
            case ItemType.Jump:
                player.jumpSpeed += player.jumpSpeed * 0.3;
                player.jumpBuffEndTime = Date.now() + GameDefine.speedBuffTime;

                message = {
                    title: SocketMessageTitle.PlayerUpdateStatus,
                    data: {
                        playerID: player.playerID,
                        updateType: PlayerUpdateType.Jump,
                        enable: true,
                    }
                }

                this.game.sendMessageToGame(message);
                break;
            case ItemType.MP5:
                this.setGun(player, WeaponType.MP5)
                break;
            case ItemType.AK47:
                this.setGun(player, WeaponType.AK47)
                break;
            case ItemType.FireGun:
                this.setGun(player, WeaponType.FireGun)
                break;
            case ItemType.Rocket:
                this.setGun(player, WeaponType.Rocket)
                break;
        }
    }

    public setGun(player: player, weaponType: WeaponType) {
        player.gunType = weaponType;
        player.gunBuffEndTime = Date.now() + GameDefine.gunBuffTime;

        let fireRate = GameDefine.fireRateDefault;

        switch (weaponType) {
            case WeaponType.MP5:
                fireRate = fireRate / 3;
                break;
            case WeaponType.AK47:
                fireRate = fireRate / 2;
                break;
            case WeaponType.Rocket:
                fireRate = fireRate * 2;
                break;
        }

        player.fireRate = fireRate;
        player.fireTime = fireRate;

        let message = {
            title: SocketMessageTitle.PlayerUpdateStatus,
            data: {
                playerID: player.playerID,
                updateType: PlayerUpdateType.Gun,
                type: weaponType,
            }
        }

        this.game.sendMessageToGame(message);
    }

    public takenDamage(playerID, damage, changeByPlayerID) {
        let players = this.game.players;
        let player = players.get(playerID);
        if (!player || player.isDie || player.isInvincible) return;

        player.isInvincible = true;
        player.invincibleEndTime = Date.now() + GameDefine.invincibleAfterTakeDameTime;

        if (player.shield >= damage) {
            this.changeShield(player, -damage);
        } else {
            let remain = damage - player.shield;
            this.changeShield(player, -damage);
            this.changeHealth(player, -remain);
        }

        if (player.health <= 0) {
            players.get(changeByPlayerID).kill += 1;
            player.die += 1;

            this.updateRanking();
            this.die(playerID);
        }
    }

    public changeHealth(player: player, change) {
        player.health += change;
        player.health = GameUtil.clamp(player.health, 0, GameDefine.maxHealth);

        let message: typeof MockSocketMessage.ON_PLAYER_UPDATE_HEALTH = {
            title: SocketMessageTitle.PlayerUpdateStatus,
            data: {
                playerID: player.playerID,
                updateType: PlayerUpdateType.Health,
                health: player.health,
            }
        }

        this.game.sendMessageToGame(message);
    }

    public changeShield(player: player, change) {
        player.shield += change;
        player.shield = GameUtil.clamp(player.shield, 0, GameDefine.maxShield);

        let message: typeof MockSocketMessage.ON_PLAYER_UPDATE_SHIELD = {
            title: SocketMessageTitle.PlayerUpdateStatus,
            data: {
                playerID: player.playerID,
                updateType: PlayerUpdateType.Shield,
                shield: player.shield,
            }
        }

        this.game.sendMessageToGame(message);
    }

    public enterTeleport(playerID: string, teleport: teleport) {
        let players = this.game.players;
        let player = players.get(playerID);
        if (!player || player.isDie) return;

        if (player.readyTeleport) {
            let timeTeleport = GameDefine.teleportTime;
            player.readyTeleport = false;
            player.isImmove = true;
            player.immoveEndTime = Date.now() + timeTeleport;
            player.isInvincible = true;
            player.invincibleEndTime = Date.now() + timeTeleport;

            setTimeout(() => {
                this.move(teleport.destination.x, teleport.destination.y, playerID, true);
                player.isJumping = false;
                player.isDroping = false;
            }, timeTeleport)

            let message: typeof MockSocketMessage.ON_PLAYER_ENTER_TELEPORT = {
                title: SocketMessageTitle.PlayerEnterTeleport,
                data: {
                    ...teleport,
                    timeTeleport
                }
            }

            this.game.sendMessageToGame(message);
        }
    }

    public die(playerID: string) {
        let player = this.game.players.get(playerID);
        if (!player || player.isDie) return;
        player.isDie = true;
        //remove
        let mapData = this.game.mapData;

        GameUtil.removeByID(mapData[Math.ceil(player.x)][Math.ceil(player.y)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.ceil(player.x)][Math.floor(player.y)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.floor(player.x)][Math.ceil(player.y)].playerIDs, playerID);
        GameUtil.removeByID(mapData[Math.floor(player.x)][Math.floor(player.y)].playerIDs, playerID);

        setTimeout(() => {
            this.respawn(playerID);
        }, 3000)
    }

    public respawn(playerID) {
        if (!this.game) return;
        let player = this.game.players.get(playerID);
        let randomIndex = GameUtil.randomRangeInt(0, MockData.playerPosition.length);
        let randomPostion = MockData.playerPosition[randomIndex];
        
        player.x = randomPostion.x;
        player.y = randomPostion.y;
        player.isJumping = false;
        player.isDroping = false;
        player.health = GameDefine.maxHealth;
        player.shield = 0;
        player.isDie = false;
        player.readyTeleport = true;
        player.isImmove = false;
        player.isInvincible = false;
        // player.isInvincible = true;
        // player.invincibleEndTime = Date.now() + GameDefine.invincibleAfterRespawnTime;

        this.game.mapData[player.x][player.x].playerIDs.push(playerID);

        let message: typeof MockSocketMessage.ON_PLAYER_RESPAWN = {
            title: SocketMessageTitle.PlayerRespawn,
            data: {
                ...player
            }
        }

        this.game.sendMessageToGame(message);
    }

    public updateRanking() {
        let data: typeof MockSocketMessage.ON_RANKING_UPDATE.data = [];

        for (let [playerID, player] of this.game.players) {
            if (player)
                data.push({
                    playerID,
                    name: player.name,
                    kill: player.kill,
                    die: player.die
                });
        }

        data.sort((a, b) => {
            if (a.kill != b.kill) {
                return b.kill - a.kill
            } else {
                return a.die - b.die;
            }
        });

        let message: typeof MockSocketMessage.ON_RANKING_UPDATE = {
            title: SocketMessageTitle.RankingUpdate,
            data
        }

        this.game.sendMessageToGame(message);
    }

    public move(x, y, playerID, isTeleport = false) {
        if (!this.game) return;
        this.setPlayerMapData(x, y, playerID);

        //send to client
        let message: typeof MockSocketMessage.ON_PLAYER_MOVE = {
            title: SocketMessageTitle.PlayerMove,
            data: {
                playerID,
                x,
                y,
                isTeleport
            }
        }

        this.game.sendMessageToGame(message);
    }

    public findPlayer(playerID) {
        let player = this.game.players.get(playerID);
        if (!player || player.isDie) return false;
        return player;
    }
}