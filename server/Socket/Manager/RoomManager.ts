import { MockData } from './../MockData';
import { Player } from "../Component/Player";
import { Room } from "../Component/Room";
import { SocketMessageTitle } from "../Defines";
import { MockSocketMessage } from "../MockSocketMessage";
import { PlayerManager } from "./PlayerManager";
import { SocketManager } from "./SocketManager";

export class RoomManager {
    private rooms: Map<string, Room> = new Map();

    public static instance: RoomManager = null;

    constructor() {
        RoomManager.instance = this;
        this.rooms = new Map();
    }

    public getRoom(roomID: string): Room {
        return this.rooms.get(roomID);
    }

    public setRoom(roomID: string, room: Room) {
        this.rooms.set(roomID, room);
    }

    public createRoom(player: Player) {
        let room = new Room(player.playerID);
        this.rooms.set(room.roomID, room);
        PlayerManager.instance.playerJoinRoom(player.playerID, room.roomID);

        let message: typeof MockSocketMessage.ON_CREATE_ROOM = {
            title: SocketMessageTitle.CreateRoom,
            data: room.handleRoomDataToClientRoom()
        }

        SocketManager.instance.sendMessageToPlayer(message, player.playerID);

        this.updateLobbyRoomForAllPlayerInLobby();
    }

    public removeRoom(roomID: string) {
        this.rooms.delete(roomID);
    }

    public quickJoinRoom(player: Player) {
        this.createRoom(player);
    }

    public joinRoom(roomID: string, playerID: string) {
        let room = this.getRoom(roomID);
        if (!room) return false;
        if (room.playerIDs.length >= room.maxUser || room.isPlaying) return false;

        room.playerIDs.push(playerID);
        PlayerManager.instance.playerJoinRoom(playerID, roomID);

        //send message join room to all player in room for update
        let message1: typeof MockSocketMessage.ON_UPDATE_ROOM = {
            title: SocketMessageTitle.UpdateRoom,
            data: room.handleRoomDataToClientRoom()
        }
        room.sendMessageToAllExceptPlayer(message1, playerID);

        //send message join room to player join room
        let message2 = {
            title: SocketMessageTitle.JoinRoom,
            data: room.handleRoomDataToClientRoom()
        }
        SocketManager.instance.sendMessageToPlayer(message2, playerID);

        this.updateLobbyRoomForAllPlayerInLobby();
    }

    public handleMessage(message, roomID) {
        let room = this.getRoom(roomID);
        if (!room) return;

        room.handleMessage(message);
    }

    public joinAvaiableRoom(player: Player) {
        let isJoined = false;
        for(let [roomID, room] of this.rooms)
        {
            if(!room.isFull)
            {
                this.joinRoom(roomID, player.playerID);
                isJoined = true;
                break;
            }
        }

        // if(!isJoined) this.createRoom(player);
    }

    public getLobbyRoomInfo() : typeof MockData.lobbyRoomInfoData
    {
        let data: typeof MockData.lobbyRoomInfoData = [];
        for(let [roomID, room] of this.rooms)
        {
            data.push(room.handleRoomDataToClientLobbyRoom());
        }
        return data;
    }

    public updateLobbyRoomForPlayer(playerID: string)
    {
        let message = {
            title: SocketMessageTitle.UpdateLobbyRoom,
            data: this.getLobbyRoomInfo()
        }

        SocketManager.instance.sendMessageToPlayer(message, playerID);
    }

    public updateLobbyRoomForAllPlayerInLobby()
    {
        let message = {
            title: SocketMessageTitle.UpdateLobbyRoom,
            data: this.getLobbyRoomInfo()
        }

        SocketManager.instance.sendMessageToAllPlayerInLobby(message);
    }

    public stopGame(roomID)
    {
        let room = this.getRoom(roomID);
        if (!room) return;

        room.stopGame();
    }
}

