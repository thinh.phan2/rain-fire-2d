import { PlayerManager } from './PlayerManager';
import { Utils } from "../../Utils";
import { Game } from "../Component/Game";
import { RoomManager } from './RoomManager';

export class GameManager {
    private games: Map<string, Game> = new Map();

    public static instance: GameManager = null;

    constructor() {
        GameManager.instance = this;
        this.games = new Map();
    }

    public getGame(gameID: string): Game {
        return this.games.get(gameID);
    }

    public setGame(gameID: string, game: Game) {
        this.games.set(gameID, game);
    }

    public startGame(roomID: string) {
        let gameID = Utils.uuid
        let game = new Game(gameID, roomID);
        this.games.set(gameID, game);
        return gameID;
    }

    public stopGame(gameID: string)
    {
        let game = this.getGame(gameID);
        if(!game) return;

        game.players.forEach(player => {
            PlayerManager.instance.playerOutGame(player.playerID);
        })

        RoomManager.instance.stopGame(game.roomID);

        this.games.delete(gameID);
    }

    public handleMessage(message, gameID) {
        let game = this.getGame(gameID);
        if (!game) return;

        //not handle, game will receive message in queue
        game.receiveMessage(message);
    }
}

