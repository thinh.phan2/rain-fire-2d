import { playerInfo } from './../Interfaces';
import { Player } from "../Component/Player";

export class PlayerManager {
    private playerInfos: Map<string, Player>;

    public static instance: PlayerManager = null;

    constructor() {
        PlayerManager.instance = this;
        this.playerInfos = new Map();
    }

    public getPlayer(playerID: string): Player {
        return this.playerInfos.get(playerID);
    }

    public setPlayer(playerID, player: Player) {
        this.playerInfos.set(playerID, player);
    }

    public connectPlayerSocket(player: playerInfo, socketID: string) {
        let playerInfo = this.getPlayer(player.playerID);

        if (!playerInfo) {
            let newPlayerInfo = new Player(player, socketID);
            this.setPlayer(newPlayerInfo.playerID, newPlayerInfo);
        } else {
            playerInfo.socketID = socketID;
        }
    }

    public playerJoinRoom(playerID, roomID) {
        let player = this.getPlayer(playerID);
        player?.joinRoom(roomID);
    }

    public playerOutRoom(playerID) {
        let player = this.getPlayer(playerID);
        player?.outRoom();
    }

    public playerJoinGame(playerIDs: string[], gameID: string) {
        playerIDs.forEach(playerID => {
            let player = this.getPlayer(playerID);
            player?.joinGame(gameID);
        })
    }

    public playerOutGame(playerID) {
        let player = this.getPlayer(playerID);
        player?.outGame();
    }

    public getPlayerRoomInfo(playerID): playerInfo {
        let player = this.getPlayer(playerID);
        if (player) {
            return {
                playerID: player.playerID,
                name: player.name
            }
        }
        return null;
    }

    public handleMessage(message, playerID) {
        let player = this.getPlayer(playerID);
        if (!player) return;
        player.handleMessage(message);
    }

    public getAllPlayerInLobby(): string[] {
        let playerIDs = []
        for (let [playerID, player] of this.playerInfos) {
            if (player.isInLobby) {
                playerIDs.push(playerID);
            }
        }

        return playerIDs;
    }

    public changeNamePlayer(playerID, name) {
        let player = this.getPlayer(playerID);
        if (!player) return;
        player.name = name;
    }
}

