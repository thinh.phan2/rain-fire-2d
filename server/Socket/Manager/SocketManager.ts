import { GameManager } from './GameManager';
import { RoomManager } from './RoomManager';
import { PlayerManager } from './PlayerManager';
import { playerInfo } from './../Interfaces';
import { Server, Socket } from "socket.io";
import { CustomSocket } from "../Component/CustomSocket";
import { SocketMessageTitle } from '../Defines';

export class SocketManager {
    public static instance: SocketManager = null;

    private _io: Server = null;
    private customSockets: Map<string, CustomSocket> = new Map();

    constructor(io) {
        SocketManager.instance = this;
        this._io = io;

        new PlayerManager();
        new RoomManager();
        new GameManager();
    }

    //set socket function
    public socketFunction(io: Server, socket: Socket) {
        socket.on(SocketMessageTitle.PlayerConnect, (player: playerInfo) => {
            this.initCustomSocket(socket, player.playerID);
            this.connectPlayerSocket(player, socket.id);
        });

        socket.on("disconnect", () => {
            console.log("user disconnected " + socket.id);
            this.destroyCustomSocket(socket.id);
        });
    }

    private initCustomSocket(socket: Socket, playerID) {
        let customSocket = new CustomSocket(socket, playerID);
        this.customSockets.set(socket.id, customSocket);
    }

    private connectPlayerSocket(player: playerInfo, socketID: string) {
        PlayerManager.instance.connectPlayerSocket(player, socketID)
    }

    private destroyCustomSocket(socketID) {
        this.customSockets.delete(socketID);
    }

    public sendMessageToPlayer(message, playerID) {
        let player = PlayerManager.instance.getPlayer(playerID);
        if (!player) return;

        this._io.to(player.socketID).emit(message.title, message.data);
    }

    public sendMessageToRoomExceptPlayer(message, playerID) {
        let player = PlayerManager.instance.getPlayer(playerID);
        if (!player) return;

        let customSocket = this.customSockets.get(player.socketID)
        if (!customSocket) return;

        customSocket.socket.to(player.roomID).emit(message.title, message.data);
    }

    public sendMessageToRoom(message, roomID) {
        this._io.to(roomID).emit(message.title, message.data);
    }

    public sendMessageToAllPlayerInLobby(message) {
        let playerIDs = PlayerManager.instance.getAllPlayerInLobby();

        playerIDs.forEach(playerID => {
            let player = PlayerManager.instance.getPlayer(playerID);
            if (!player) return;

            this._io.to(player.socketID).emit(message.title, message.data);
        })
    }

    public addSocketToRoom(socketID, roomID) {
        let customSocket = this.customSockets.get(socketID)
        if (!customSocket) return;

        console.log("SOCKET JOIN ROOM: " + customSocket.socket.id + " " + roomID);
        customSocket.socket.join(roomID);
    }

    public removeSocketFromRoom(socketID, roomID) {
        let customSocket = this.customSockets.get(socketID)
        if (!customSocket) return;

        customSocket.socket.leave(roomID);
    }

    public addSocketToGame(socketID, gameID) {
        let customSocket = this.customSockets.get(socketID)
        if (!customSocket) return;

        console.log("SOCKET JOIN GAME: " + customSocket.socket.id + " " + gameID);
        customSocket.socket.join(gameID);
    }

    public removeSocketFromGame(socketID, gameID) {
        let customSocket = this.customSockets.get(socketID)
        if (!customSocket) return;

        customSocket.socket.leave(gameID);
    }

    public handleMessage(message) {
        switch (message.title) {
            //message from lobby
            case SocketMessageTitle.CreateRoom:
                console.log(message.data)
                RoomManager.instance.createRoom(message.data);
                break;
            case SocketMessageTitle.JoinRoom:
                RoomManager.instance.joinRoom(message.data.roomID, message.data.playerID);
                break;
            case SocketMessageTitle.JoinAvaiableRoom:
                RoomManager.instance.joinAvaiableRoom(message.data);
                break;
            case SocketMessageTitle.UpdateLobbyRoom:
                console.log("RECEIVED UPDATE ROOM MESSAGE");
                RoomManager.instance.updateLobbyRoomForPlayer(message.data.playerID);
                break;
            case SocketMessageTitle.PlayerChangeName:
                PlayerManager.instance.changeNamePlayer(message.data.playerID, message.data.name);
                break;
        }
    }
}