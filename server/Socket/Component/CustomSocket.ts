import { PlayerManager } from './../Manager/PlayerManager';
import { Socket } from 'socket.io';
export class CustomSocket {
    private _playerID: string = null;
    private _socket: Socket = null;

    constructor(socket: Socket, playerID: string) {
        this._playerID = playerID;
        this._socket = socket;

        console.log("a user connected ", socket.id, this.playerID);

        socket.onAny((title, data) => {
            let message = {
                title,
                data
            }

            this.handleMessage(message);
        });
    }

    public get socket(): Socket {
        return this._socket;
    }

    public set socket(v: Socket) {
        this._socket = v;
    }

    public get playerID(): string {
        return this._playerID;
    }

    public set playerID(v: string) {
        this._playerID = v;
    }

    public handleMessage(message) {
        //send message to player manager to handle it
        PlayerManager.instance.handleMessage(message, this._playerID);
    }
}