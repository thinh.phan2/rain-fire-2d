import { GameManager } from './../Manager/GameManager';
import { SocketManager } from './../Manager/SocketManager';
import { playerInfo } from "../Interfaces";
import { RoomManager } from '../Manager/RoomManager';

export enum PlayerState {
    Lobby,
    Room,
    Playing
}

export class Player {
    private _playerID: string;
    private _name: string;
    private _roomID: string;
    private _socketID: string;
    private _state: PlayerState;
    private _gameID: string;

    constructor(player: playerInfo, socketID) {
        this._playerID = player.playerID;
        this._name = player.name;
        this._socketID = socketID;
        this._roomID = null;
        this._state = PlayerState.Lobby;
    }

    /* #region  get set function */
    public get playerID(): string {
        return this._playerID;
    }

    public set playerID(v: string) {
        this._playerID = v;
    }

    public get name(): string {
        return this._name;
    }

    public set name(v: string) {
        this._name = v;
    }

    public get roomID(): string {
        return this._roomID;
    }

    public set roomID(v: string) {
        this._roomID = v;
    }

    public get gameID(): string {
        return this._gameID;
    }

    public set gameID(v: string) {
        this._gameID = v;
    }

    public get socketID(): string {
        return this._socketID;
    }

    public set socketID(v: string) {
        this._socketID = v;
    }

    public get state(): PlayerState {
        return this._state;
    }

    public set state(v: PlayerState) {
        this._state = v;
    }

    public get isInLobby()
    {
        return this._state === PlayerState.Lobby;
    }

    public get isInRoom()
    {
        return this._state === PlayerState.Room;
    }

    public get isInGame()
    {
        return this._state === PlayerState.Playing;
    }
    
    /* #endregion */

    public joinRoom(roomID) {
        this._roomID = roomID;
        SocketManager.instance.addSocketToRoom(this.socketID, this._roomID);
        this._state = PlayerState.Room;
    }

    public outRoom() {
        SocketManager.instance.removeSocketFromRoom(this.socketID, this._roomID);
        this._roomID = null;
        this._state = PlayerState.Lobby;
    }

    public joinGame(gameID) {
        this._gameID = gameID;
        SocketManager.instance.addSocketToGame(this.socketID, this._gameID);
        this._state = PlayerState.Playing;
    }

    public outGame() {
        SocketManager.instance.removeSocketFromGame(this.socketID, this._gameID);
        this._gameID = null;
        this._state = PlayerState.Room;
    }

    public handleMessage(message) {
        switch (this._state) {
            case PlayerState.Lobby:
                SocketManager.instance.handleMessage(message);
                break;
            case PlayerState.Room:
                if(this._roomID)
                {
                    RoomManager.instance.handleMessage(message, this.roomID);
                }else{
                    console.log("Player don't in any room! " + this.playerID);
                }
                break;
            case PlayerState.Playing:
                if(this._gameID)
                {
                    GameManager.instance.handleMessage(message, this.gameID);
                }else{
                    console.log("Player don't in any game! " + this.playerID);
                }
                break;
        }
    }
}

