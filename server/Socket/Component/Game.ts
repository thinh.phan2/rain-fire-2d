import { GameUtil } from './../GameController/GameUtil';
import { SocketManager } from './../Manager/SocketManager';
import { PlayerManager } from './../Manager/PlayerManager';
import { GameItemController } from './../GameController/GameItemController';
import { GameCollisionController } from './../GameController/GameCollisionController';
import { GameBulletController } from './../GameController/GameBulletController';
import { Utils } from "../../Utils";
import { GamePlayerController } from "../GameController/GamePlayerController";
import { bullet, cell, item, player, Queue, teleport } from "../Interfaces";
import { GameDefine, SocketMessageTitle, WeaponType } from '../Defines';
import { RoomManager } from '../Manager/RoomManager';
import { MockData } from '../MockData';
import { GameManager } from '../Manager/GameManager';

export class Game {
    private _gameID: string;
    private _roomID: string;

    private preMessages;
    private queue: Queue<Object> = null;
    private handleGameMessageLoop = null;
    private handleGameUpdate = null;

    public mapData: cell[][];
    public players: Map<string, player>;
    public bullets: Map<string, bullet>;
    public items: Map<string, item>;
    public teleports: Map<string, teleport>;
    public timer = 0;
    public currentTime: number = 0;
    public endTime = 0;

    public get roomID(): string {
        return this._roomID;
    }

    //controller
    public gamePlayerController: GamePlayerController;
    public gameBulletController: GameBulletController;
    public gameCollisionController: GameCollisionController;
    public gameItemController: GameItemController;

    constructor(gameID: string, roomID: string) {
        this._gameID = gameID;
        this._roomID = roomID;

        let room = RoomManager.instance.getRoom(this._roomID);
        PlayerManager.instance.playerJoinGame(room.playerIDs, this._gameID);

        this.gamePlayerController = new GamePlayerController(this._gameID);
        this.gameBulletController = new GameBulletController(this._gameID);
        this.gameCollisionController = new GameCollisionController(this._gameID);
        this.gameItemController = new GameItemController(this._gameID);
        this.startGame();
    }

    private startGame() {
        this.sendStartGameMessage(); // send message start game to all client
        this.initData();
        this.startGameLoop();
        this.initMapData();
    }

    private startGameLoop() {
        this.handleGameMessageLoop = setInterval(() => {
            this.checkMessage();
        }, GameDefine.timePerFrame);

        this.handleGameUpdate = setInterval(() => {
            this.updateGameNextStatus();
        }, GameDefine.timePerFrame);
    }

    private stopGameLoop() {
        this.handleGameMessageLoop && clearInterval(this.handleGameMessageLoop);
        this.handleGameUpdate && clearInterval(this.handleGameUpdate);
    }

    private destroyGameController() {
        delete this.gamePlayerController;
        delete this.gameBulletController;
        delete this.gameCollisionController;
        delete this.gameItemController;
    }

    public initData() {
        this.mapData = [];
        this.players = new Map();
        this.bullets = new Map();
        this.items = new Map();
        this.teleports = new Map();
        this.preMessages = [];
        this.queue = new Queue<Object>();
    }

    private checkMessage() {
        for (let i = 0; i < GameDefine.maxPreMessage; i++) {
            if (this.preMessages.length >= GameDefine.maxPreMessage) break;
            let message = this.queue.dequeue();
            if (message) {
                this.preMessages.push(message);
            } else {
                break;
            }
        }
        this.handlePreMessage();
    }

    private handlePreMessage() {
        this.preMessages.forEach(message => {
            let player;
            switch (message.title) {
                //message from game
                case SocketMessageTitle.PlayerMove:
                    player = this.gamePlayerController.findPlayer(message.data.playerID)
                    if (!player) break;

                    if (message.data.isJump) {
                        player.jumpMessage = message;
                    } else {
                        player.moveMessage = message;
                    }
                    break;
                case SocketMessageTitle.PlayerShot:
                    player = this.gamePlayerController.findPlayer(message.data.playerID)
                    if (!player) break;

                    player.shotMessage = message;
                    break;
            }
        })
        this.preMessages = [];
    }


    private updateGameNextStatus() {
        if (this.endTime > Date.now()) {
            this.timer -= GameDefine.timePerFrame;
            this.currentTime = Date.now();
            this.gamePlayerController.updatePlayerNextStatus();
            this.gameBulletController.updateBulletNextStatus();
            this.gameCollisionController.checkCollision();
        } else {
            this.endGame();
        }
    }

    public handleMessage(message) {
        let title = message.title;
        switch (title) {
            case SocketMessageTitle.PlayerMove:
                this.gamePlayerController.playerMove(message.data);
                break;
            case SocketMessageTitle.PlayerShot:
                this.gameBulletController.playerShot(message.data);
                break;
        }
    }

    //init
    private initMapData() {
        for (let i = 0; i < GameDefine.height; i++) {
            this.mapData[i] = []
            for (let j = 0; j < GameDefine.width; j++) {
                this.mapData[i][j] = { tile: MockData.map[GameDefine.width * (GameDefine.height - 1 - i) + j] }
                this.mapData[i][j].playerIDs = [];
                this.mapData[i][j].bulletIDs = [];
                this.mapData[i][j].itemIDs = [];
                this.mapData[i][j].teleportIDs = [];
            }
        }

        //player data
        let room = RoomManager.instance.getRoom(this._roomID);
        room.playerIDs.forEach(playerID => {
            let randomIndex = GameUtil.randomRangeInt(0, MockData.playerPosition.length);
            let randomPostion = MockData.playerPosition[randomIndex];

            let player = PlayerManager.instance.getPlayer(playerID);
            let playerData: typeof MockData.player = {
                playerID: player.playerID,
                name: player.name,
                ...randomPostion
            };

            this.initPlayer(playerData);
        })

        //item data
        MockData.gameItem.forEach(itemData => {
            this.initItem(itemData);
        });

        //teleport data
        MockData.gameTeleport.forEach(teleportData => {
            let id1 = Utils.uuid;
            let id2 = Utils.uuid;
            this.initTeleport(id1, id2, teleportData.position1, teleportData.position2, teleportData.color);
            this.initTeleport(id2, id1, teleportData.position2, teleportData.position1, teleportData.color);
        });

        this.timer = this.timer + GameDefine.gameTime;
        this.endTime = Date.now() + GameDefine.gameTime;

        let message = {
            title: SocketMessageTitle.InitMap,
            data: {
                mapData: this.mapData,
                players: Array.from(this.players),
                items: Array.from(this.items),
                teleports: Array.from(this.teleports),
                width: GameDefine.width,
                heigth: GameDefine.height,
                timer: GameDefine.gameTime
            }
        };

        this.sendMessageToGame(message);
    }

    private initPlayer(playerData: typeof MockData.player) {
        this.mapData[playerData.x][playerData.y].playerIDs.push(playerData.playerID);
        let player: player = {
            playerID: playerData.playerID,
            name: playerData.name,
            x: playerData.x,
            y: playerData.y,
            direction: playerData.direction,
            health: GameDefine.maxHealth,
            shield: 0,
            isJumping: false,
            isDroping: false,
            kill: 0,
            die: 0,
            isDie: false,
            speed: GameDefine.playerSpeed,
            jumpHigh: GameDefine.playerJumpHigh,
            jumpSpeed: GameDefine.playerJumpSpeed,
            dropSpeed: GameDefine.playerDropSpeed,
            readyTeleport: true,
            speedBuffEndTime: 0,
            jumpBuffEndTime: 0,
            fireRate: GameDefine.fireRateDefault,
            fireTime: GameDefine.fireRateDefault,
            gunBuffEndTime: 0,
            gunType: WeaponType.Pistol,
            isImmove: false,
            immoveEndTime: 0,
            isInvincible: false,
            invincibleEndTime: 0
        }

        this.players.set(playerData.playerID, player);
    }

    private initItem(itemData) {
        let itemID = Utils.uuid;
        this.mapData[itemData.x][itemData.y].itemIDs.push(itemID);
        let item: item = {
            itemID,
            ...itemData,
            isEnable: true
        }
        this.items.set(itemID, item);
    }

    private initTeleport(id, desID, position, destination, color) {
        this.mapData[position.x][position.y].teleportIDs.push(id);
        let teleport: teleport = {
            teleportID: id,
            destinationID: desID,
            position,
            destination,
            color
        }
        this.teleports.set(id, teleport);
    }

    public sendMessageToGame(message) {
        SocketManager.instance.sendMessageToRoom(message, this._gameID);
    }

    public receiveMessage(message) {
        this.queue.enqueue(message);
    }

    private sendStartGameMessage() {
        let message = {
            title: SocketMessageTitle.StartGame,
            data: {
                gameID: this._gameID
            }
        }
        this.sendMessageToGame(message);
    }

    private endGame() {
        this.stopGameLoop();
        this.destroyGameController();

        let rankingPlayers = [];

        for (let [playerID, player] of this.players) {
            rankingPlayers.push({
                playerID: player.playerID,
                name: player.name,
                kill: player.kill,
                die: player.die
            })
        }

        rankingPlayers.sort((a, b) => {
            if (a.kill != b.kill) {
                return b.kill - a.kill
            } else {
                return a.die - b.die;
            }
        });

        let message = {
            title: SocketMessageTitle.EndGame,
            data: {
                rankingPlayers
            }
        }

        this.sendMessageToGame(message);

        GameManager.instance.stopGame(this._gameID);
    }
}

