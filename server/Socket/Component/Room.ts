import { GameManager } from './../Manager/GameManager';
import { Utils } from "../../Utils";
import { SocketMessageTitle } from "../Defines";
import { PlayerManager } from "../Manager/PlayerManager";
import { SocketManager } from "../Manager/SocketManager";
import { MockData } from "../MockData";
import { MockSocketMessage } from "../MockSocketMessage";
import { RoomManager } from '../Manager/RoomManager';

export class Room {
    private _roomID: string;
    private _hostPlayerID: string;
    private _maxUser: number;
    private _playerIDs: string[];
    private _isPlaying: boolean;
    private _gameID: string;

    constructor(hostPlayerID) {
        this._hostPlayerID = hostPlayerID;
        this._roomID = Utils.uuid;
        this._maxUser = 10;
        this._playerIDs = [];
        this.playerIDs.push(hostPlayerID);
        this._isPlaying = false;
    }

    public get roomID(): string {
        return this._roomID;
    }

    public set roomID(v: string) {
        this._roomID = v;
    }

    public get hostPlayerID(): string {
        return this._hostPlayerID;
    }

    public set hostPlayerID(v: string) {
        this._hostPlayerID = v;
    }

    public get maxUser(): number {
        return this._maxUser;
    }

    public set maxUser(v: number) {
        this._maxUser = v;
    }

    public get playerIDs(): string[] {
        return this._playerIDs;
    }

    public get isFull(): boolean {
        return this.playerIDs.length >= this.maxUser;
    }

    public get gameID(): string {
        return this._gameID;
    }

    public set gameID(v: string) {
        this._gameID = v;
    }

    public get isPlaying(): boolean {
        return this._isPlaying;
    }

    public set isPlaying(v: boolean) {
        this._isPlaying = v;
    }

    public addPlayer(playerID) {
        this._playerIDs.push(playerID);
    }

    public handleMessage(message) {
        switch (message.title) {
            case SocketMessageTitle.KickOutRoom:
                this.kickRoom(message.data.playerID);
                break;
            case SocketMessageTitle.OutRoom:
                this.outRoom(message.data.playerID);
                break;
            case SocketMessageTitle.StartGame:
                this.startGame();
                break;
        }
    }

    public kickRoom(kickPlayerID) {
        this.playerIDs.splice(this.playerIDs.findIndex(e => e === kickPlayerID), 1);

        let message1: typeof MockSocketMessage.ON_UPDATE_ROOM = {
            title: SocketMessageTitle.UpdateRoom,
            data: this.handleRoomDataToClientRoom()
        }
        this.sendMessageToAllExceptPlayer(message1, kickPlayerID);

        //player out room after send message, because of need on room to send message
        PlayerManager.instance.playerOutRoom(kickPlayerID);

        let message2 = {
            title: SocketMessageTitle.KickOutRoom,
            data: {
                playerID: kickPlayerID
            }
        }
        SocketManager.instance.sendMessageToPlayer(message2, kickPlayerID);

        RoomManager.instance.updateLobbyRoomForAllPlayerInLobby();
    }

    public outRoom(outPlayerID) {
        this.playerIDs.splice(this.playerIDs.findIndex(e => e === outPlayerID), 1);

        if (this.playerIDs.length == 0) {
            RoomManager.instance.removeRoom(this.roomID);
        } else {
            if (this._hostPlayerID == outPlayerID) {
                this._hostPlayerID = this.playerIDs[0];
            }

            let message1: typeof MockSocketMessage.ON_UPDATE_ROOM = {
                title: SocketMessageTitle.UpdateRoom,
                data: this.handleRoomDataToClientRoom()
            }
            this.sendMessageToAllExceptPlayer(message1, outPlayerID);
        }

        //player out room after send message, because of need on room to send message
        PlayerManager.instance.playerOutRoom(outPlayerID);

        let message2 = {
            title: SocketMessageTitle.OutRoom,
            data: {
                playerID: outPlayerID
            }
        }
        SocketManager.instance.sendMessageToPlayer(message2, outPlayerID);

        RoomManager.instance.updateLobbyRoomForAllPlayerInLobby();
    }

    public handleRoomDataToClientRoom(): typeof MockData.roomInfoData {
        let data: typeof MockData.roomInfoData;
        let users = [];

        this.playerIDs.forEach(playerID => {
            let playerInfo = PlayerManager.instance.getPlayerRoomInfo(playerID);
            if (playerInfo) users.push(playerInfo);
        })
        data = {
            roomID: this.roomID,
            hostPlayerID: this.hostPlayerID,
            maxUser: this.maxUser,
            users
        }

        return data;
    }

    public handleRoomDataToClientLobbyRoom(): typeof MockData.lobbyRoomInfoData[0] {
        let data: typeof MockData.lobbyRoomInfoData[0];

        data = {
            roomID: this.roomID,
            currentUserCounter: this.playerIDs.length,
            maxUser: this.maxUser,
            mapIndex: 1,
            isPlaying: this._isPlaying
        }

        return data;
    }

    public sendMessageToAllPlayer(message) {
        SocketManager.instance.sendMessageToRoom(message, this.roomID);
    }

    public sendMessageToAllExceptPlayer(message, playerID) {
        SocketManager.instance.sendMessageToRoomExceptPlayer(message, playerID);
    }

    public startGame() {
        // if(this._playerIDs.length > 1)
        {
            let gameID = GameManager.instance.startGame(this._roomID);
            this._gameID = gameID;
            this._isPlaying = true;
            RoomManager.instance.updateLobbyRoomForAllPlayerInLobby();
        }
    }

    public stopGame()
    {
        this._gameID = null;
        this._isPlaying = false;
        RoomManager.instance.updateLobbyRoomForAllPlayerInLobby();
    }
}

