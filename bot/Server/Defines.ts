export enum BotType {
    JoinRoom,
    CreateRoom
}

export enum WeaponType {
    Pistol,
    MP5,
    AK47,
    FireGun,
    Rocket
}

export enum ItemType {
    Health,
    Shield,
    Speed,
    Jump,
    Trap,
    MP5,
    AK47,
    FireGun,
    Rocket
}

export enum PlayerUpdateType {
    Health,
    Shield,
    Speed,
    Jump,
    Trap,
    Gun,
    Die,
    Respawn
}

export enum SocketMessageTitle {
    PlayerConnect = "player-connect",
    JoinRoom = "join-room",
    QuickJoinRoom = "quick-join-room",
    CreateRoom = "create-room",
    UpdateRoom = "update-room",
    KickOutRoom = "kick-out-room",
    OutRoom = "out-room",
    StartGame = "start-game",
    InitMap = "init-map",
    PlayerMove = "player-move",
    PlayerShot = "player-shot",
    PlayerClaimItem = "player-claim-item",
    PlayerEnterTeleport = "player-enter-teleport",
    PlayerUpdateStatus = "player-update-status",
    PlayerRespawn = "player-respawn",
    RankingUpdate = "ranking-update",
    ItemRespawn = "item-respawn",
    EndGame = "end-game",
    UpdateLobbyRoom = "update-lobby-room",
    JoinAvaiableRoom = "Join-avaiable-room"
}

export class GameDefine {
    public static readonly FPS = 60;
    public static readonly timePerFrame = 1000 / this.FPS;
    public static readonly playerSpeed = 10 / this.FPS;
    public static readonly playerJumpHigh = 5;
    public static readonly playerJumpSpeed = 12 / this.FPS;
    public static readonly playerDropSpeed = 8 / this.FPS;
    public static readonly bulletSpeed = 20 / this.FPS;
    public static readonly buttetDistance = 16;
    public static readonly width = 70;
    public static readonly height = 38;
    public static readonly unit = 1;

    public static readonly fixedFloat = 2;

    public static readonly maxHealth = 6;
    public static readonly maxShield = 3;

    public static readonly speedBuffTime = 20 * 1000; //20s
    public static readonly jumpBuffTime = 20 * 1000; //20s

    public static readonly fireRateDefault = 0.3 * 1000; //0.3s
    public static readonly gunBuffTime = 20 * 1000; //20s

    public static readonly teleportTime = 0.5 * 1000;

    public static readonly invincibleAfterTakeDameTime = 0.5 * 1000;
    public static readonly invincibleAfterRespawnTime = 3 * 1000;

    public static readonly gameTime = 5 * 60 * 1000;

    public static readonly maxPreMessage = 20;
}