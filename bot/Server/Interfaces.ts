import { ItemType, WeaponType } from "./Defines"

export interface playerInfo {
    playerID: string,
    name: string
}

export interface cell {
    tile: number,
    playerIDs?: string[],
    bulletIDs?: string[],
    itemIDs?: string[],
    teleportIDs?: string[],
    trapIDs?: boolean
}

export interface player {
    playerID: string,
    name: string,
    x: number,
    y: number,
    xRespawn: number,
    yRespawn: number,
    direction: number,
    health: number,
    shield: number,
    isDroping?: boolean,
    isJumping?: boolean,
    xJumping?: number,
    moveMessage?: Object,
    jumpMessage?: Object,
    shotMessage?: Object
    kill: number,
    die: number,
    isDie: boolean,
    speed: number,
    jumpHigh: number,
    jumpSpeed: number,
    dropSpeed: number,
    readyTeleport: boolean,
    speedBuffEndTime: number,
    jumpBuffEndTime: number,
    fireRate: number,
    fireTime: number,
    gunBuffEndTime: number,
    gunType: WeaponType,
    isImmove: boolean,
    immoveEndTime: number,
    isInvincible: boolean,
    invincibleEndTime: number
}

export interface bullet {
    playerID: string,
    bulletID: string,
    x: number,
    y: number,
    xPre: number,
    yPre: number,
    w: number,
    h: number,
    direction: number,
    yDestination: number,
    isDestroy: boolean,
    speed: number,
    distance: number,
    damage: number
}

export interface item {
    itemID: string,
    type: ItemType,
    x: number,
    y: number,
    isEnable: boolean
}

export interface teleport {
    teleportID: string,
    destinationID: string,
    position: {
        x: number,
        y: number
    },
    destination: {
        x: number,
        y: number
    },
    color: {
        x: number,
        y: number,
        z: number
    }
}

interface IQueue<T> {
    enqueue(item: T): void;
    dequeue(): T | undefined;
    size(): number;
}

export class Queue<T> implements IQueue<T> {
    private storage: T[] = [];

    constructor(private capacity: number = Infinity) { }

    enqueue(item: T): void {
        if (this.size() === this.capacity) {
            throw Error("Queue has reached max capacity, you cannot add more items");
        }
        this.storage.push(item);
    }
    dequeue(): T | undefined {
        return this.storage.shift();
    }
    size(): number {
        return this.storage.length;
    }
    reset() {
        this.storage = [];
    }
}
