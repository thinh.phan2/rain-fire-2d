import { BotManager } from './Bot/BotManager';
import express from "express"
import http from "http"

const app = express();
const server = http.createServer(app);

new BotManager();

app.get("/", (req, res) => {
    res.send("123");
});

server.listen(4000, () => {
    console.log("listening on *:4000");
});