import { BotManager } from './BotManager';
import { io, Socket } from "socket.io-client";
import { SocketMessageTitle } from "../Server/Defines";
import { playerInfo } from "../Server/Interfaces";


enum SocketState {
    CLOSED,
    ERROR,
    CONNECTING,
    OPEN,
}

export class SocketClient {
    private _socket: Socket = null;
    private _botID: string = null;
    private _state: SocketState = null;

    public get socket(): Socket {
        return this._socket;
    }

    public set socket(v: Socket) {
        this._socket = v;
    }

    public get botID(): string {
        return this._botID;
    }

    public set botID(v: string) {
        this._botID = v;
    }

    public onMessage: Function = null;

    public get isReady() {
        return this._socket && this._state === WebSocket.OPEN;
    }

    constructor(botID: string) {
        this._botID = botID;
        this.connect();
    }

    public connect() {
        console.log("[SOCKET] ws send request connect");
        this.socket = io("http://localhost:3000");
        this._state = SocketState.CONNECTING;
        this.setSocketEvent();
    }

    private setSocketEvent() {
        this.socket.on("connect", () => {
            this.onOpen();
        });

        this.socket.on("connect_error", () => {
            this.onError();
        });

        this.socket.on("disconnect", () => {
            this.onClose();
        });

        this.socket.onAny((title, data) => {
            let message = {
                title,
                data
            }

            if (this.onMessage != null) {
                this.onMessage(message);
            }
        })
    }

    private onOpen() {
        console.log("[SOCKET] ws is connected! " + this.socket.id);

        let bot = BotManager.instance.getBot(this.botID);
        if (!bot) return;

        let player: playerInfo = {
            playerID: bot.botID,
            name: bot.name
        }

        let message = {
            title: SocketMessageTitle.PlayerConnect,
            data: player
        }

        this.sendMessageToServer(message);

        this._state = SocketState.OPEN;
    }

    private onClose() {
        console.log("[SOCKET] ws is closed!");
        this._state = SocketState.CLOSED;
    }

    private onError() {
        console.log("[SOCKET] ws is error!");
        this._state = SocketState.ERROR;
        // setTimeout(() => {
        //     this.socket.connect();
        // }, 1000);
    }

    public sendMessageToServer(message) {
        this.socket.emit(message.title, message.data);
    }
}

