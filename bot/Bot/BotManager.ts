import { MockData } from './../Server/MockData';
import { playerInfo } from '../Server/Interfaces';
import { Bot } from './Bot';
import { Utils } from '../Server/Utils';

export class BotManager {
    private bots: Map<string, Bot>;

    public static instance: BotManager = null;
    private botDatas: typeof MockData.gameBot = []

    constructor() {
        BotManager.instance = this;
        this.bots = new Map();
        this.initBotData();
        this.initBot();
    }

    public getBot(botID: string): Bot {
        return this.bots.get(botID);
    }

    public setPlayer(botID, bot: Bot) {
        this.bots.set(botID, bot);
    }

    public handleMessage(message, botID) {
        let bot = this.getBot(botID);
        if (!bot) return;
        bot.handleMessage(message);
    }

    public initBot() {
        // MockData.gameBot.forEach(botData => {
        //     this.initBotFromBotData(botData);
        // })
        this.botDatas.forEach(botData => {
            this.initBotFromBotData(botData);
        });
    }

    private initBotFromBotData(botData: typeof MockData.gameBot[0]) {
        let playerInfo: playerInfo = {
            playerID: botData.playerID,
            name: botData.name
        }

        let bot = new Bot(playerInfo, botData.type, botData.timeLobbyAction);
        this.bots.set(bot.botID, bot);
    }

    public initBotData() {
        MockData.gameBot2.forEach(botDefine => {
            for (let i = 0; i < botDefine.amout; i++) {
                let botData: typeof MockData.gameBot[0] = {
                    playerID: Utils.uuid,
                    name: Utils.getRandomName(),
                    type: botDefine.type,
                    timeLobbyAction: 1000
                }

                this.botDatas.push(botData);
            }
        })
    }
}