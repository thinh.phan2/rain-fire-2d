import { Utils } from './../Server/Utils';
import { GameDefine, SocketMessageTitle } from './../Server/Defines';
import { SocketClient } from './SocketClient';
import { playerInfo } from "../Server/Interfaces";
import { BotType } from '../Server/Defines';
import { MockSocketMessage } from '../Server/MockSocketMessage';

export enum BotState {
    Lobby,
    Room,
    Playing
}

export enum BotActionInGame {
    MoveRight,
    MoveLeft,
    Jump,
    Shot
}

export class Bot {
    private _botID: string;
    private _name: string;
    private _socket: SocketClient;
    private _state: BotState;
    private _type: BotType;
    private _timeLobbyAction: number;

    private _direction = 1;

    private lobbyLoop = null;
    private roomLoop = null;
    private gameLoop = null;

    constructor(player: playerInfo, type: BotType, timeLobbyAction: number) {
        this._botID = player.playerID;
        this._name = player.name;
        this._socket = new SocketClient(this._botID);
        this._socket.onMessage = this.handleMessage.bind(this);
        this._state = BotState.Lobby;
        this._type = type;
        this._timeLobbyAction = timeLobbyAction;

        this.runLobbyLoop();
    }

    public get botID(): string {
        return this._botID;
    }

    public set botID(v: string) {
        this._botID = v;
    }

    public get name(): string {
        return this._name;
    }

    public set name(v: string) {
        this._name = v;
    }

    public get socket(): SocketClient {
        return this._socket;
    }

    public set socket(v: SocketClient) {
        this._socket = v;
    }

    public get state(): BotState {
        return this._state;
    }

    public set state(v: BotState) {
        this._state = v;
    }

    public get type(): BotType {
        return this._type;
    }

    public set type(v: BotType) {
        this._type = v;
    }

    public handleMessage(mesasge) {
        switch (mesasge.title) {
            case SocketMessageTitle.JoinRoom:
            case SocketMessageTitle.CreateRoom:
                console.log(this._botID + " join room");
                this.setSate(BotState.Room);
                break;
            case SocketMessageTitle.OutRoom:
                console.log(this._botID + " out room");
                this.setSate(BotState.Lobby);
                break;
            case SocketMessageTitle.KickOutRoom:
                console.log(this._botID + " is kicked room");
                this.setSate(BotState.Lobby);
                break;
            case SocketMessageTitle.StartGame:
                console.log(this._botID + " is playing game");
                this.setSate(BotState.Playing);
                break;
        }
    }

    public sendMessage(message) {
        if (!this.socket) return;
        this.socket.socket.emit(message.title, message.data);
    }

    private setSate(state: BotState) {
        this._state = state;

        switch (state) {
            case BotState.Lobby:
                this.runLobbyLoop();
                break;
            case BotState.Room:
                this.runRoomLoop();
                break;
            case BotState.Playing:
                this.runGameLoop();
                break;
        }
    }

    private runLobbyLoop() {
        this.gameLoop && clearInterval(this.gameLoop);
        this.roomLoop && clearInterval(this.roomLoop);

        this.lobbyLoop = setInterval(() => {
            switch (this._type) {
                case BotType.JoinRoom:
                    if (this.state == BotState.Lobby) {
                        //send message to join room
                        let message = {
                            title: SocketMessageTitle.JoinAvaiableRoom,
                            data: {
                                playerID: this._botID,
                                name: this._name
                            }
                        }
                        this.sendMessage(message);
                    }
                    break;                    
                case BotType.CreateRoom:
                    if (this.state == BotState.Lobby) {
                        //send message to join room
                        let message = {
                            title: SocketMessageTitle.CreateRoom,
                            data: {
                                playerID: this._botID,
                                name: this._name
                            }
                        }
                        this.sendMessage(message);
                    }
                    break;
            }
        }, this._timeLobbyAction)
    }

    private runRoomLoop() {
        this.lobbyLoop && clearInterval(this.lobbyLoop);
        this.gameLoop && clearInterval(this.gameLoop);

        this.roomLoop = setInterval(() => {
            switch (this._type) {                
                case BotType.CreateRoom:
                    if (this.state == BotState.Room) {
                        //send message to start game
                        let message = {
                            title: SocketMessageTitle.StartGame,
                            data: {}
                        }
                        this.sendMessage(message);
                    }
                    break;
            }
        }, 5000)
    }

    private runGameLoop() {
        this.lobbyLoop && clearInterval(this.lobbyLoop);

        this.roomLoop && clearInterval(this.roomLoop);
        this.selectBotActionIngame();
    }

    private selectBotActionIngame() {
        let botActionsIngame = [BotActionInGame.MoveRight, BotActionInGame.MoveLeft, BotActionInGame.Jump, BotActionInGame.Shot];
        let action = botActionsIngame[Utils.randomRangeInt(0, botActionsIngame.length)];
        this.runBotActionLoop(action);

    }

    private runBotActionLoop(action: BotActionInGame) {
        let duration = 3000;

        switch (action) {
            case BotActionInGame.MoveRight:
                duration = Utils.randomRangeInt(1000, 2000);
                break;
            case BotActionInGame.MoveLeft:
                duration = Utils.randomRangeInt(1000, 2000);
                break;
            case BotActionInGame.Jump:
                duration = Utils.randomRangeInt(200, 300);
                break;
            case BotActionInGame.Shot:
                duration = Utils.randomRangeInt(3000, 5000);
                break;
        }

        this.gameLoop = setInterval(() => {
            this.runBotAction(action);
            duration -= GameDefine.timePerFrame;
            if (duration <= 0) {
                clearInterval(this.gameLoop)
                this.selectBotActionIngame();
            }
        }, GameDefine.timePerFrame);
    }

    private runBotAction(action: BotActionInGame) {
        switch (action) {
            case BotActionInGame.MoveRight:
                this._direction = 1;
                this.onMove(false);
                break;
            case BotActionInGame.MoveLeft:
                this._direction = -1;
                this.onMove(false);
                break;
            case BotActionInGame.Jump:
                this.onMove(true);
                break;
            case BotActionInGame.Shot:
                this.onShot();
                break;
        }
    }

    private onMove(isJump = false) {
        let data: typeof MockSocketMessage.SEND_PLAYER_MOVE.data = {
            playerID: this._botID,
            direction: this._direction,
            isJump
        }

        this.sendMessage({
            title: SocketMessageTitle.PlayerMove,
            data
        });
    }

    private onShot()
    {
        this.sendMessage({
            title: SocketMessageTitle.PlayerShot,
            data: {
                playerID: this.botID
            }
        });
    }
}