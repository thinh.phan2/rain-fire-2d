import { _decorator, Component, log } from "cc";
const { ccclass, property } = _decorator;

declare global {
    interface Window {
        MaxApi: any;
        OnHideLoading: any;
        realtimeSinceStartup: any;
    }
}

@ccclass('MaxApiUtils')
export class MaxApiUtils extends Component {

    public static request(name: string, ...params: any[]) {
        log("[MaxApiUtils]", "Call function: ", name);
        return new Promise((resolve, reject) => {
            if (window.MaxApi) {
                let fnc = window.MaxApi[name];
                if (fnc) {
                    if (params && params.length > 0) {
                        fnc(params, function (res: any) {
                            resolve?.(res);
                        });
                    }
                    else {
                        fnc(function (res: any) {
                            resolve?.(res);
                        });
                    }
                }
            }
        });
    }

    public static saveImage(data: string) {
        return new Promise((resolve, resject) => {
            if (window.MaxApi) {
                window.MaxApi.requestPermission('storage', (status: string) => {
                    if (status === 'granted') {
                        window.MaxApi.saveImage(data, (result: boolean) => {
                            if (result) {
                                window.MaxApi.showToast({
                                    duration: 5000,
                                    title: 'Lưu hình ảnh thành công',
                                });

                                resolve?.(true);

                            } else {
                                window.MaxApi.showToast({
                                    duration: 5000,
                                    title: 'Lưu hình ảnh không thành công',
                                });
                                resolve?.(false);
                            }
                        });
                    } else {
                        window.MaxApi.showToast({
                            duration: 5000,
                            title: 'Lưu hình ảnh không thành công',
                        });
                        resolve?.(false);
                    }
                });
            }
            else {
                resolve?.(false);
            }
        });
    }

    // static registerScreenShot(callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.listen("onScreenShot", () => {
    //             callback();
    //         })
    //     }
    //     else {
    //         callback(null);
    //     }
    // }

    // static RegisterNoti(callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.observer('DIS_RECEIVE_NOTI', (res: any) => {
    //             console.log(`[DIS_RECEIVE_NOTI] ${res}`);
    //             callback(res);
    //         });
    //     }
    //     else {
    //         callback(null);
    //     }
    // }

    // static GetProfile(): Promise<void> {
    //     return new Promise((resolve, reject) => {
    //         if (window.MaxApi) {
    //             window.MaxApi.getProfile(function (res: any) {
    //                 resolve(res);
    //             });
    //         }
    //         else {
    //             resolve();
    //         }
    //     });
    // }

    // static UploadImage(base64: string): Promise<string> {
    //     return new Promise<string>((resolve, reject) => {
    //         const props = {
    //             path: 'base64-upload',
    //             files: base64,
    //             options: {
    //                 loading: true,
    //             },
    //         };
    //         if (window.MaxApi) {
    //             window.MaxApi.uploadImage(props, ({ status, response }) => {
    //                 const { url } = response;
    //                 if (url && url.length > 0 && url.indexOf('http') >= 0) {
    //                     resolve(url);
    //                 } else {
    //                     resolve(response);
    //                 }
    //             });
    //         }
    //         else {
    //             resolve(null);
    //         }
    //     })
    // }

    // static GetDeviceInfo() {
    //     return new Promise((resolve, reject) => {
    //         if (window.MaxApi) {
    //             window.MaxApi.getDeviceInfo(function (res: any) {
    //                 resolve(res);
    //             });
    //         }
    //         else {
    //             resolve(null);
    //         }
    //     });
    // }

    // static CheckHighPerformanceDevice() {
    //     return new Promise((resolve, reject) => {
    //         if (window.MaxApi) {
    //             window.MaxApi.isHighPerformanceDevice(function (res: any) {
    //                 resolve(res);
    //             });
    //         }
    //         else {
    //             resolve(true);
    //         }
    //     });
    // }

    // static closeGame() {
    //     if (window.MaxApi) {
    //         window.MaxApi.dismiss();
    //     }
    // }

    // static copyToClipboard(copyText: string, toastMsg: string) {
    //     if (window.MaxApi) {
    //         window.MaxApi.copyToClipboard(copyText, toastMsg);
    //     }
    // }

    // static openWeb(url: string) {
    //     window.MaxApi.openWeb({ url })
    // }

    // //start screen by feature code.
    // static startFeatureCode(featureCode: string, params: any, callback: Function) {
    //     if (!window.MaxApi) {
    //         callback(true);
    //     }
    //     else {
    //         window.MaxApi.startFeatureCode(featureCode, params, callback);
    //     }
    // }

    // static checkPermission(permissionName: string): Promise<string> {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve('granted');
    //         }
    //         else {
    //             window.MaxApi.checkPermission(permissionName, (result: string) => {
    //                 resolve(result)
    //             });
    //         }
    //     })
    // }

    // static requestPermission(permissionName: string): Promise<string> {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve('granted');
    //         }
    //         else {
    //             window.MaxApi.requestPermission(permissionName, (result: string) => {
    //                 resolve(result)
    //             });
    //         }
    //     });
    // }

    // static getContacts(): Promise<Object> {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve([]);
    //         }
    //         else {
    //             const paramsContact = {
    //                 allowNonMomo: false,
    //                 autoFocus: true,
    //                 isAllowMerchant: false,
    //                 showPopupNonMomo: false,
    //                 allowAgency: false,
    //                 allowMultipleSelection: true,
    //             };
    //             window.MaxApi.getContacts({ paramsContact, title: "MegaLuckyWheel" },
    //                 (contacts: Object) => {
    //                     resolve(contacts)
    //                 }
    //             );
    //         }
    //     })
    // }

    // static avatarEndpoint: string = "";
    // static getAvatarEndPoint(): Promise<string> {

    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve("");
    //         }
    //         else {
    //             if (this.avatarEndpoint.length > 0) {
    //                 resolve(this.avatarEndpoint);
    //             }
    //             window.MaxApi.getAvatarEndPoint((response: string) => {
    //                 this.avatarEndpoint = response;
    //                 resolve(response)
    //             });
    //         }
    //     })
    // }

    // static getContact(): Promise<any[]> {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve([]);
    //         }
    //         else {
    //             window.MaxApi.getContact({},
    //                 (contacts: []) => {
    //                     resolve(contacts)
    //                 }
    //             );
    //         }
    //     })
    // }

    // static goBack(): Promise<string> {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve('denied')
    //         }
    //         else {
    //             window.MaxApi.goBack((result: string) => {
    //                 resolve(result)
    //             });
    //         }
    //     })
    // }

    // static getScreenShot(callback: Function) {
    //     if (!window.MaxApi) {
    //         callback("");
    //     }
    //     else {
    //         window.MaxApi.getScreenShot(callback);
    //     }
    // }
    // static saveImage(data: string) {
    //     return new Promise((resolve) => {
    //         if (window.MaxApi) {
    //             window.MaxApi.requestPermission('storage', (status: string) => {
    //                 if (status === 'granted') {
    //                     window.MaxApi.saveImage(data, (result: boolean) => {
    //                         if (result) {
    //                             window.MaxApi.showToast({
    //                                 duration: 5000,
    //                                 title: 'Lưu hình ảnh thành công',
    //                             });

    //                             resolve(true);

    //                         } else {
    //                             window.MaxApi.showToast({
    //                                 duration: 5000,
    //                                 title: 'Lưu hình ảnh không thành công',
    //                             });
    //                             resolve(false);
    //                         }
    //                     });
    //                 } else {
    //                     window.MaxApi.showToast({
    //                         duration: 5000,
    //                         title: 'Lưu hình ảnh không thành công',
    //                     });
    //                     resolve(false);
    //                 }
    //             });
    //         }
    //         else {
    //             resolve(false);
    //         }
    //     })

    // }

    // static shareFacebook(params: object, callback: Function) {
    //     if (!window.MaxApi) {
    //         callback("");
    //     }
    //     else {
    //         window.MaxApi.shareFacebook(params, callback);
    //     }
    // }

    // static facebookMsg(link: string) {
    //     const _url = "fb-messenger://share/?link=" + link;
    //     if (window.MaxApi) {
    //         window.MaxApi.openURL(_url);
    //     }
    // };

    // static moreMenu(title: string, subject: string, message: string, url: string) {
    //     return new Promise((resolve) => {
    //         if (!window.MaxApi) {
    //             resolve(false);
    //         }
    //         else {
    //             let shareOptions = {
    //                 title,
    //                 message,
    //                 url,
    //                 subject,
    //             };
    //             window.MaxApi.share(shareOptions, (result: boolean) => {
    //                 resolve(result);
    //             });
    //         }
    //     });
    // }

    // static copyLink(link: string) {
    //     if (window.MaxApi) {
    //         window.MaxApi.copyToClipboard(link, 'Đã sao chép');
    //     }
    // }

    // static trackEvent(params: {}) {
    //     if (window.MaxApi) {
    //         window.MaxApi.trackEvent('momo_jump', params);
    //     }
    // }

    // static showToast(title: string, time: number = 3000) {
    //     if (window.MaxApi) {
    //         window.MaxApi.showToast({
    //             duration: time,
    //             title
    //         });
    //     }
    // }

    // /////////////////Tracking performent/////////////////////
    // static startTraceScreenLoad(screenName: string, callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.startTraceScreenLoad(screenName, ({ traceId }) => {
    //             callback(traceId);
    //         });
    //     }
    //     else {
    //         callback();
    //     }
    // }
    // static startTraceScreenInteraction(screenName: string, callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.startTraceScreenInteraction(screenName, ({ traceId }) => {
    //             callback(traceId);
    //         });
    //     }
    //     else {
    //         callback();
    //     }
    // }
    // static startTraceScreenGoal(screenName: string, callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.startTraceScreenGoal(screenName, ({ traceId }) => {
    //             callback(traceId);
    //         });
    //     }
    //     else {
    //         callback();
    //     }
    // }
    // static startTrace(flow: string, step: string, callback: Function) {
    //     if (window.MaxApi) {
    //         window.MaxApi.startTrace({flow, step}, ({ traceId }) => {
    //             callback(traceId);
    //         });
    //     }
    //     else {
    //         callback();
    //     }
    // }
    // static traceSuccess(traceId) {
    //     if (window.MaxApi) {
    //         window.MaxApi.traceSuccess(traceId, {});
    //     }
    // }
    // static traceFail(traceId) {
    //     if (window.MaxApi) {
    //         window.MaxApi.traceFail(traceId, {});
    //     }
    // }
    // static stopTrace(traceId) {
    //     if (window.MaxApi) {
    //         window.MaxApi.stopTrace(traceId, {});
    //     }
    // }
}