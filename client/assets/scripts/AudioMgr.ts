
import { _decorator, Component, AudioSource, AudioClip, AssetManager, resources } from 'cc';
import EventListener from './EventListener';
import { SoundEvent, SoundName } from './game/define';
import { Utils } from './Utils';
const { ccclass, property } = _decorator;

@ccclass('AudioMgr')
export class AudioMgr extends Component {

    private audioClips: AudioClip[] = [];
    private bgVolume: number = 0.4;
    private sfxVolume: number = 0.7;

    audioSourceSFX: AudioSource = null;
    audioSourceMusic: AudioSource = null;

    public isOn: boolean = true;

    onLoad() {
        this.onEvent();
        this.audioSourceSFX = this.addComponent(AudioSource);
        this.audioSourceSFX.loop = false;
        this.audioSourceSFX.volume = this.sfxVolume;
        this.audioSourceSFX.playOnAwake = false;
        this.audioSourceMusic = this.addComponent(AudioSource);
        this.audioSourceMusic.playOnAwake = false;
        this.audioSourceMusic.play();
    }

    onEvent() {
        EventListener.on(SoundEvent.Load, this.load, this);
        EventListener.on(SoundEvent.OnOff, this.onOff, this);
        EventListener.on(SoundEvent.PlaySfx, this.playSfx, this);
        EventListener.on(SoundEvent.PlayMusic, this.PlayMusic, this);
        EventListener.on(SoundEvent.StopSfx, this.stopSfx, this);
        EventListener.on(SoundEvent.Stop, this.stop, this);
    }

    private PlayOneShot(clip: AudioClip, isOneShot: boolean = false) {
        if (this.audioSourceSFX.playing) {
            this.audioSourceSFX.stop()
        }
        if (clip && this.isOn) {
            if (!isOneShot) {
                this.audioSourceSFX.clip = clip;
                this.audioSourceSFX.play();
            } else {
                this.audioSourceSFX.playOneShot(clip);
            }
        }
    }

    private PlayClip(clip: AudioClip, loop: boolean) {
        if (clip && this.isOn) {
            if (this.audioSourceMusic.clip != clip || !this.audioSourceMusic.playing) {
                this.audioSourceMusic.stop();
                this.audioSourceMusic.clip = clip;
                this.audioSourceMusic.loop = loop;

                this.audioSourceMusic.play();
                this.audioSourceMusic.volume = this.bgVolume;
                this.audioSourceMusic.play();
            }

        }
    }

    private stop() {
        this.audioSourceMusic.stop();
        this.audioSourceSFX.stop();
    }

    private load() {
        let array = Object.keys(SoundName);
        array.forEach(element => {
            let name = SoundName[element];
            Utils.getAudioRes("sounds/" + name, audioClip => {
                if (audioClip) {
                    audioClip.name = name;
                    this.audioClips.push(audioClip);
                    console.log("Sound load: " + name);
                }
                else {
                    console.error("Sound notfound: " + name);
                }
            });
        });
    }

    private playSfx(name: SoundName, isOneShot: boolean = false) {
        let clip = this.audioClips.find(clip => clip.name == name);
        if (clip) {
            this.PlayOneShot(clip, isOneShot);
        }
    }

    private stopSfx(name: SoundName) {
        this.audioClips.forEach(clip => {
            if (clip.name == name && this.audioSourceSFX.clip == clip) {
                if (this.audioSourceSFX.playing) {
                    this.audioSourceSFX.stop();
                }
            }
            return;
        });
    }

    private PlayMusic(name: SoundName, loop: boolean) {
        this.audioClips.forEach(clip => {
            if (clip.name == name) {
                this.PlayClip(clip, loop);
            }
            return;
        });
    }

    private onOff() {
        this.isOn = !this.isOn;
        if (!this.isOn) {
            this.stop();
        }
        else {
            // this.PlayMusic(SoundName.Bgm, true);
        }
    }
}

