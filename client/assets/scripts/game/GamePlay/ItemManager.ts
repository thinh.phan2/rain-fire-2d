
import { _decorator, Component, Node, Prefab, instantiate, Vec3 } from 'cc';
import { ItemControllder } from './ItemControllder';
import { Utils } from '../../Utils';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('ItemManager')
export class ItemManager extends Component {
    @property(Prefab) itemPrefab: Prefab = null;

    public spawnsItem(items) {
        for (let [itemID, itemData] of items) {
            let item = instantiate(this.itemPrefab);
            item.active = true;
            item.setParent(this.node);
            item.name = itemID;
            item.setPosition(new Vec3(itemData.y * MockData.unitMap, itemData.x * MockData.unitMap, 0));
            item.getComponent(ItemControllder).spawn(itemData.type);
        }
    }

    private findItem(id): Node {
        for (var i = 0; i < this.node.children.length; i++) {
            if (this.node.children[i].name === id) {
                return this.node.children[i];
            }
        }
        return null;
    }

    public destroyItem(itemID){
        let item = this.findItem(itemID);
        if(item)
        {
            item.active = false;
        }
    }

    public respawnItem(itemData)
    {
        let item = this.findItem(itemData.itemID);
        if(item)
        {
            item.active = true;
            item.getComponent(ItemControllder).spawn(itemData.type)
        }
    }

    public reset()
    {
        Utils.destroyAllChild(this.node);
    }
}

