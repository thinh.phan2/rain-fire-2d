import { _decorator, Component, Node, Animation } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('EffectController')
export class EffectController extends Component {
    @property(Animation) animController: Animation = null;
    @property(Node) shield: Node  = null;
    @property(Node) health: Node = null;

    public playShieldEffect()
    {
        this.shield.active = true;
        this.animController.play("shield");
        setTimeout(() => {
            this.shield.active = false;
        }, 830)
    }

    public playHealthEffect()
    {
        this.health.active = true;
        this.animController.play("health");

        setTimeout(() => {
            this.health.active = false;
        }, 850)
    }
}

