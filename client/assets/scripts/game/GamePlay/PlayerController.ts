import { GameEvent, Layer, PlayerUpdateType, ServerDefine, SocketMessageTitle, WeaponType, SoundEvent, SoundName } from './../define';
import { UIGamePlayEvent } from '../define';
import { _decorator, Component, input, Input, KeyCode, Vec3, Node, Prefab, instantiate, math, Tween, tween, Collider2D, Contact2DType, IPhysics2DContact, BoxCollider2D, Animation, RigidBody2D, ERigidBody2DType, Label } from 'cc';
import { WeaponController } from './WeaponController';
import EventListener from '../../EventListener';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { Profile } from '../Profile';
import { MockData } from '../../../data/MockData';
import { SocketManager } from '../../Network/SocketManager';
import { EffectController } from './EffectController';
const { ccclass, property } = _decorator;

enum State {
    IDLE,
    RUN,
    JUMP,
    FALL,
    HURT,
    DEAD
}

@ccclass('PlayerController')
export class PlayerController extends Component {
    @property(WeaponController) weaponController: WeaponController = null;
    @property(Prefab) trapPrefab: Prefab = null;
    @property(Animation) animController: Animation = null;
    @property(Label) nameLabel: Label = null;
    @property(Node) model: Node = null;
    @property(EffectController) effectController: EffectController = null;
    // @property(Node) buttonLeft: Node = null;
    // @property(Node) buttonRight: Node = null;
    // @property(Node) buttonJump: Node = null;
    // @property(Node) buttonFire: Node = null;
    // @property(Node) buttonTrap: Node = null;


    private _playerID = '';

    private direction = 1;
    private isMoving = false;
    private isOnGround = true;

    private health = 6;
    private shield = 0;
    private isHasTrap = false;

    private tweenMove: Tween<Object> = null;

    private state: State = State.IDLE;

    public get playerID() {
        return this._playerID;
    }

    public set playerID(id: string) {
        this._playerID = id;
    }

    public get isMainPlayer() {
        return this.playerID === Profile.instance.userID;
    }

    setEvent() {
        if (this.isMainPlayer) {
            input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);
            input.on(Input.EventType.KEY_PRESSING, this.onKeyPress, this);
            input.on(Input.EventType.KEY_UP, this.onKeyUp, this);

            // this.buttonLeft.on(Node.EventType.TOUCH_START, this.onMoveLeft, this);
            // this.buttonRight.on(Node.EventType.TOUCH_START, this.onMoveRight, this);

            // this.buttonLeft.on(Node.EventType.TOUCH_END, this.onMoveEnd, this);
            // this.buttonRight.on(Node.EventType.TOUCH_END, this.onMoveEnd, this);

            // this.buttonJump.on(Node.EventType.TOUCH_START, this.onJump, this);
            // this.buttonFire.on(Node.EventType.TOUCH_START, this.onFire, this);
            // this.buttonTrap.on(Node.EventType.TOUCH_START, this.onSetTrap, this);
            input.on(Input.EventType.MOUSE_DOWN, this.onFire, this);

            // EventListener.emit(SoundEvent.PlayMusic, SoundName.Playing);
        }
    }

    private onKeyDown(event) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
            case KeyCode.ARROW_LEFT:
                this.onMoveLeft();
                this.isMoving = true;
                break;
            case KeyCode.KEY_D:
            case KeyCode.ARROW_RIGHT:
                this.onMoveRight();
                this.isMoving = true;
                break;
            case KeyCode.SPACE:
            case KeyCode.ARROW_UP:
            case KeyCode.KEY_W:
                this.onJump();
                break;
        }
    }

    private onKeyPress(event) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
            case KeyCode.ARROW_LEFT:
                this.onMoveLeft();
                this.isMoving = true;
                break;
            case KeyCode.KEY_D:
            case KeyCode.ARROW_RIGHT:
                this.onMoveRight();
                this.isMoving = true;
                break;
        }
    }

    private onKeyUp(event) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
            case KeyCode.ARROW_LEFT:
            case KeyCode.KEY_D:
            case KeyCode.ARROW_RIGHT:
                this.isMoving = false;
                this.setAnimation(State.IDLE);
                break;
        }
    }

    update(deltaTime: number) {
        if (this.direction == -1) {
            this.model.scale = new Vec3(-1, 1, 1);
        } else if (this.direction == 1) {
            this.model.scale = new Vec3(1, 1, 1);
        }

        if (this.isMainPlayer) {
            if (this.direction != 0 && this.isMoving) {
                this.onMove(false);
            }
        }
    }

    public reset() {
        this.node.active = true;
        this.shield = 0;
        this.isHasTrap = false;
        EventListener.emit(UIGamePlayEvent.UpdateShield, this.shield);
        EventListener.emit(UIGamePlayEvent.UpdateTrap, this.isHasTrap);
    }

    public respawn(data: typeof MockSocketMessage.ON_PLAYER_RESPAWN.data) {
        this.node.active = true;
        this.setPosition(data);
        this.setHealth(data.health);

        if (this.isMainPlayer) {
            EventListener.emit(UIGamePlayEvent.Respawn);
            EventListener.emit(SoundEvent.PlaySfx, SoundName.Respawn);
        }
    }

    private onMoveLeft() {
        this.direction = -1;
    }

    private onMoveRight() {
        this.direction = 1;
    }

    private onMoveEnd() {
        this.direction = 0;
    }

    private onJump() {
        this.onMove(true);
        EventListener.emit(SoundEvent.PlaySfx, SoundName.Jump);
    }

    private onFire() {
        this.weaponController.onFire(this.playerID);
    }

    private onSetTrap() {
        if (this.isHasTrap && this.isOnGround) {
            this.isHasTrap = false;
            let trap = instantiate(this.trapPrefab);
            trap.setParent(this.node.parent);
            trap.setPosition(this.node.position);
            EventListener.emit(UIGamePlayEvent.UpdateTrap, this.isHasTrap);
        }
    }

    public claimItem() {
        if (this.isMainPlayer) {
            EventListener.emit(SoundEvent.PlaySfx, SoundName.Claim_Item);
        }
    }

    private onMove(isJump = false) {
        let data: typeof MockSocketMessage.SEND_PLAYER_MOVE.data = {
            playerID: this.playerID,
            direction: this.direction,
            isJump
        }

        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.PlayerMove,
            data
        });

        if (isJump) {
            this.setAnimation(State.JUMP);
        } else {
            this.setAnimation(State.RUN);
        }
    }

    public setPosition(data) {
        let newPosition = new Vec3(data.y * MockData.unitMap, data.x * MockData.unitMap, 0);
        this.node.setPosition(newPosition);
        EventListener.emit(GameEvent.UpdateCamera);
    }

    public setPostionFollowServer(data: typeof MockSocketMessage.ON_PLAYER_MOVE.data) {

        if (data.isTeleport) {
            this.setPosition(data);
            return;
        }

        let newPosition = new Vec3(data.y * MockData.unitMap, data.x * MockData.unitMap, 0);

        if (!this.isMainPlayer) {
            if (this.node.position.x !== newPosition.x)
                this.direction = this.node.position.x < newPosition.x ? 1 : -1
        }

        if (this.tweenMove) {
            this.tweenMove.stop();
        }

        this.tweenMove = tween(this.node).to(ServerDefine.timePerFrame, { position: newPosition }, {
            onUpdate: (target, ratio) => {
                if (this.isMainPlayer) {
                    EventListener.emit(GameEvent.UpdateCamera);
                }
            }
        }).start();
    }

    public setName(name: string) {
        this.nameLabel.string = name;
    }

    public setBullet(data: typeof MockSocketMessage.ON_PLAYER_SHOT.data) {
        this.weaponController.setBullet(data);
    }

    public setHealth(health) {
        if (this.health < health) {
            this.effectController.playHealthEffect();
        }

        this.health = health;

        if (this.isMainPlayer)
            EventListener.emit(UIGamePlayEvent.UpdateHeart, this.health);
        if (this.health == 0) {
            if (this.isMainPlayer)
                EventListener.emit(UIGamePlayEvent.Die);

            this.node.active = false
        }
    }

    public setShield(shield) {
        if (shield < this.shield) {
            this.effectController.playShieldEffect();
        }

        this.shield = shield;

        if (this.isMainPlayer) {
            EventListener.emit(UIGamePlayEvent.UpdateShield, this.shield);
        }

    }

    public updateStatus(data) {
        switch (data.updateType) {
            case PlayerUpdateType.Health:
                this.setHealth(data.health);
                break;
            case PlayerUpdateType.Shield:
                this.setShield(data.shield);
                break;
            case PlayerUpdateType.Speed:
                if (data.enable == true)
                    console.log("SPEED UP")
                else
                    console.log("END BUFF SPEED")
                break;
            case PlayerUpdateType.Jump:
                if (data.enable == true)
                    console.log("JUMP UP")
                else
                    console.log("END BUFF JUMP")
                break;
            case PlayerUpdateType.Gun:
                switch (data.type) {
                    case WeaponType.Pistol:
                        this.weaponController.changeGun(WeaponType.Pistol);
                        break;
                    case WeaponType.MP5:
                        this.weaponController.changeGun(WeaponType.MP5);
                        break;
                    case WeaponType.AK47:
                        this.weaponController.changeGun(WeaponType.AK47);
                        break;
                    case WeaponType.FireGun:
                        this.weaponController.changeGun(WeaponType.FireGun);
                        break;
                    case WeaponType.Rocket:
                        this.weaponController.changeGun(WeaponType.Rocket);
                        break;
                }
                break;
        }
    }

    public hitBullet(playerShotID: string) {
        if (this.playerID == playerShotID) return; // Không thể nhận đạn từ chính mình

        if (this.shield == 0) {
            this.setAnimation(State.HURT);

            // Nếu là người chơi chính or người bắn là người chơi chính
            if (this.isMainPlayer || playerShotID == Profile.instance.userID) {
                EventListener.emit(SoundEvent.PlaySfx, SoundName.Hit);
            }
        }
    }

    private setAnimation(state: State, force = false) {
        if (this.state == state || this.state == State.HURT) {
            if (!force)
                return;
        }

        this.state = state;

        switch (state) {
            case State.IDLE:
                this.animController.play("Idle");
                break;
            case State.RUN:
                this.animController.play("Run");
                break;
            case State.JUMP:
                this.animController.play("Jump");
                break;
            case State.FALL:
                this.animController.play("Fall");
                break;
            case State.HURT:
                this.animController.play("Hurt");

                setTimeout(() => {
                    this.setAnimation(State.IDLE, true);
                }, 500)
                break;
            case State.DEAD:
                // this.animController.play("Dead");
                break;
        }
    }
}

