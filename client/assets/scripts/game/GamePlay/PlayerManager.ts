import { PlayerController } from './PlayerController';
import { _decorator, Component, Node, Prefab, instantiate, find } from 'cc';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { CameraController } from './CameraController';
import { Utils } from '../../Utils';
import { Profile } from '../Profile';
import EventListener from '../../EventListener';
import { GameEvent } from '../define';
const { ccclass, property } = _decorator;

@ccclass('PlayerManager')
export class PlayerManager extends Component {
    @property(Prefab) playerPrefabs: Prefab[] = [];

    public initPlayer(playerData) {
        let playerIndex = 0;
        if (playerData.playerID != Profile.instance.userID) {
            playerIndex = 1;
        }
        let player = instantiate(this.playerPrefabs[playerIndex]);
        player.setParent(this.node);

        let playerController = player.getComponent(PlayerController)
        playerController.playerID = playerData.playerID;
        playerController.setPosition(playerData);
        player.name = playerData.playerID;
        playerController.setName(playerData.name);

        if (playerData.playerID == Profile.instance.userID) {
            this.setCamera(playerController.node);
            playerController.setEvent();
            playerController.setHealth(playerData.health);
            playerController.reset();
        }
    }

    public respawnPlayer(data: typeof MockSocketMessage.ON_PLAYER_RESPAWN.data) {
        let player = this.findPlayer(data.playerID);
        if (player) {
            player.getComponent(PlayerController).respawn(data);
        }
    }

    private findPlayer(id): Node {
        for (var i = 0; i < this.node.children.length; i++) {
            if (this.node.children[i].name === id) {
                return this.node.children[i];
            }
        }
        return null;
    }

    public movePlayer(data: typeof MockSocketMessage.ON_PLAYER_MOVE.data) {
        let player = this.findPlayer(data.playerID);
        if (player) {
            let playerController = player.getComponent(PlayerController)
            playerController.setPostionFollowServer(data);
        }
    }

    public setCamera(player) {
        let camera = find('Canvas/Container/GamePlay/CameraHolder');
        let cameraController = camera.getComponent(CameraController);
        cameraController.Player = player;
        EventListener.emit(GameEvent.UpdateCamera);
    }

    public setBullet(data: typeof MockSocketMessage.ON_PLAYER_SHOT.data) {
        let player = this.findPlayer(data.playerID);
        if (player) {
            player.getComponent(PlayerController).setBullet(data);
        }
    }

    public updatePlayerStatus(data) {
        let player = this.findPlayer(data.playerID);
        if (player) {
            player.getComponent(PlayerController).updateStatus(data);
        }
    }

    public claimItem(playerID) {
        let player = this.findPlayer(playerID);
        if (player) {
            player.getComponent(PlayerController).claimItem();
        }
    }

    public reset() {
        Utils.destroyAllChild(this.node);
        let bulletHolder = find('Canvas/Container/GamePlay/BulletHolder');
        Utils.destroyAllChild(bulletHolder);
    }
}

