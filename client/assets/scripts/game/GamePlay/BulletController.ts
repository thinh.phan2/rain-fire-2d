import { Layer, ServerDefine } from './../define';
import { _decorator, Component, Vec3, Tween, tween, Collider2D, Contact2DType, IPhysics2DContact } from 'cc';
import { MockData } from '../../../data/MockData';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { PlayerController } from './PlayerController';
const { ccclass, property } = _decorator;

@ccclass('BulletController')
export class BulletController extends Component {
    private destination: Vec3 = null;
    private direction: number = 0;

    private isDestroy: boolean = false;
    private isPredestroy: boolean = false;

    private speed: number = 0;
    private distancePerFrame: number = 0;
    private playerShotID: string = '';

    onLoad() {
        this.getComponent(Collider2D).on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
    }

    private onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null) {
        //collision with tile or other
        if (otherCollider.group == Layer.DEFAULT || otherCollider.group == Layer.BULLET || otherCollider.group == Layer.PLAYER) {
            this.isPredestroy = true;
            this.getComponent(Collider2D).off(Contact2DType.BEGIN_CONTACT);
            this.node.children[0].active = false;

            if (otherCollider.group == Layer.PLAYER) {
                let player = otherCollider.node.getComponent(PlayerController);
                player.hitBullet(this.playerShotID);
            }

            setTimeout(() => {
                this.destroyBullet();
            }, 50);
        }
    }

    public setInfo(data: typeof MockSocketMessage.ON_PLAYER_SHOT.data) {
        this.node.active = true;
        this.node.setPosition(new Vec3(data.y * MockData.unitMap, data.x * MockData.unitMap, 0));
        this.destination = new Vec3(data.yDestination * MockData.unitMap, data.x * MockData.unitMap, 0);
        this.speed = data.speed;
        this.direction = data.direction;
        this.node.setScale(this.node.scale.x * this.direction, this.node.scale.y, this.node.scale.z);
        this.distancePerFrame = this.speed * MockData.unitMap * this.direction / ServerDefine.timePerFrame;
        this.playerShotID = data.playerID;
    }

    public destroyBullet() {
        if (this.isDestroy) return;
        this.isDestroy = true;
        this.node.parent.removeChild(this.node);
        this.node.destroy();
    }

    update(dt: number) {
        if (this.destination && !this.isPredestroy && !this.isDestroy) {
            let currentPosition = this.node.position.clone();
            currentPosition.add3f(this.distancePerFrame * dt, 0, 0);

            let isDestroy = false;
            if (this.direction == 1) {
                isDestroy = currentPosition.x >= this.destination.x;
            } else {
                isDestroy = currentPosition.x <= this.destination.x;
            }

            if (isDestroy) {
                this.destroyBullet();
            } else {
                this.node.setPosition(currentPosition);
            }
        }
    }
}

