import { _decorator, Component, Node, Sprite, SpriteFrame } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('MapCell')
export class MapCell extends Component {
    @property(Sprite) sprite: Sprite= null;
    @property(SpriteFrame) listSprite: SpriteFrame[] = [];

    setSprite(index: number)
    {
        this.sprite.spriteFrame = this.listSprite[index]
    }
}

