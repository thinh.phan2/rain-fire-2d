import { _decorator, Component, Node, SpriteFrame } from 'cc';
import { MockSocketMessage } from '../../../../FakeSocket/MockSocketMessage';
import { Profile } from '../../../Profile';
import { GamePlayRankingItem } from './GamePlayRankingItem';
const { ccclass, property } = _decorator;

@ccclass('GamePlayRanking')
export class GamePlayRanking extends Component {
    @property(SpriteFrame) listCup: SpriteFrame[] = [];

    setInfo(data: typeof MockSocketMessage.ON_RANKING_UPDATE.data) {
        let isSetMine = false;

        for (let i = 0; i < data.length; i++) {
            let isMine: boolean = data[i].playerID === Profile.instance.userID;

            if (i <= 2) {
                let node = this.node.children[i];
                node.active = true;
                if (isMine) {
                    node.getComponent(GamePlayRankingItem).setInfo(data[i], i, this.listCup[i], true);
                    isSetMine = true;
                } else {
                    node.getComponent(GamePlayRankingItem).setInfo(data[i], i, this.listCup[i], false);
                }
            } else {
                let node = this.node.children[3];
                if (isSetMine) {
                    node.active = false;
                    return;
                }
                else if (isMine) {
                    node.active = true;
                    node.getComponent(GamePlayRankingItem).setInfo(data[i], i, null, true);
                }
            }
        }
    }
}

