import { _decorator, Component, Node, Label, SpriteFrame, Sprite, Color } from 'cc';
import { MockSocketMessage } from '../../../../FakeSocket/MockSocketMessage';
const { ccclass, property } = _decorator;

@ccclass('GamePlayRankingItem')
export class GamePlayRankingItem extends Component {
    @property(Label) nameLabel: Label = null;
    @property(Label) scoreLabel: Label = null;
    @property(Label) rankLabel: Label = null;
    @property(Sprite) cupSprite: Sprite = null;
    @property(Sprite) background: Sprite = null;

    setInfo(data: typeof MockSocketMessage.ON_RANKING_UPDATE.data[0], rank: number, cupImage: SpriteFrame, isMine = false) {
        this.nameLabel.string = data.name;
        this.scoreLabel.string = data.kill.toString() + '  ' + data.die.toString();

        if (cupImage) {
            this.cupSprite.node.active = true;
            this.cupSprite.spriteFrame = cupImage;
            this.rankLabel.node.active = false;
        } else {
            this.rankLabel.node.active = true;
            this.rankLabel.string = (rank + 1).toString();
            this.cupSprite.node.active = false;
        }

        if (isMine) {
            this.background.color = Color.YELLOW;
        } else {
            this.background.color = Color.WHITE;
        }
    }
}

