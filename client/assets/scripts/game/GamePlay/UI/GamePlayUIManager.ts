import { GamePlayRanking } from './Ranking/GamePlayRanking';
import { _decorator, Component, Node, Sprite, SpriteFrame, Label, Button } from 'cc';
import EventListener from '../../../EventListener';
import { MockSocketMessage } from '../../../FakeSocket/MockSocketMessage';
import { Utils } from '../../../Utils';
import { UIGamePlayEvent } from '../../define';
const { ccclass, property } = _decorator;

@ccclass('GamePlayUIManager')
export class GamePlayUIManager extends Component {
    @property(Node) heartHolder: Node = null;
    @property(Node) shieldHolder: Node = null;
    @property(Sprite) weaponImage: Sprite = null;
    @property(Label) weaponTimerLabel: Label = null;
    @property(Label) gameTimerLabel: Label = null;
    @property(Button) trapButton: Button = null;
    @property(Node) die: Node = null;
    @property(Label) countDown: Label = null;
    @property(GamePlayRanking) ranking: GamePlayRanking = null;

    private weaponTimer = 0;
    private gameTimer = 0;

    onLoad() {
        this.setEvent();
    }

    setEvent() {
        EventListener.on(UIGamePlayEvent.UpdateHeart, this.updateHeart, this);
        EventListener.on(UIGamePlayEvent.UpdateShield, this.updateShield, this);
        EventListener.on(UIGamePlayEvent.UpdateWeapon, this.updateWeapon, this);
        EventListener.on(UIGamePlayEvent.UpdateTrap, this.updateTrap, this);
        EventListener.on(UIGamePlayEvent.Die, this.onDie, this);
        EventListener.on(UIGamePlayEvent.Respawn, this.onRespawn, this);
        EventListener.on(UIGamePlayEvent.UpdateRanking, this.updateRanking, this);
        EventListener.on(UIGamePlayEvent.UpdateTimer, this.updateTimer, this);
    }

    updateHeart(currentHeart) {
        this.heartHolder.children.forEach((item, index) => {
            if (index <= currentHeart - 1) {
                item.active = true;
            } else {
                item.active = false;
            }
        })
    }

    updateShield(currentShiled) {
        this.shieldHolder.children.forEach((item, index) => {
            if (index <= currentShiled - 1) {
                item.active = true;
            } else {
                item.active = false;
            }
        })
    }

    updateWeapon(sprite: SpriteFrame, isPistol = false) {
        this.weaponImage.spriteFrame = sprite;
        if (!isPistol) this.weaponTimer = 20;
        else {
            this.weaponTimer = 0;
        }
    }

    updateTrap(isHasTrap: boolean) {
        this.trapButton.node.active = isHasTrap;
    }

    updateTimer(timer: number) {
        this.gameTimer = timer;
    }

    update(dt: number) {
        this.updateWeaponTimer(dt);
        this.updateGameTimer(dt);
    }

    private updateWeaponTimer(dt) {
        if (this.weaponTimer > 0) {
            this.weaponTimer -= dt;
        } else {
            this.weaponTimer = 0;
        }
        if(this.weaponTimer > 0)
        {
            this.weaponTimerLabel.node.active = true;
            this.weaponTimerLabel.string = Math.floor(this.weaponTimer).toString() + ' s';
        }else{
            this.weaponTimerLabel.node.active = false;
        }
    }

    private updateGameTimer(dt) {
        if (this.gameTimer > 0) {
            this.gameTimer -= dt * 1000;
        } else {
            this.gameTimer = 0;
        }
       
        this.gameTimerLabel.string = Utils.convertMilisecondToTime(this.gameTimer);
    }

    onDie() {
        this.die.active = true;
        this.startDieCountDown();
    }

    onRespawn() {
        this.die.active = false;
    }

    startDieCountDown() {
        let time = 3;
        this.countDown.string = time.toString();

        let coutDownInterval = setInterval(() => {
            time--;
            this.countDown.string = time.toString();

            if (time == 0)
                clearInterval(coutDownInterval);
        }, 1000)
    }

    updateRanking(data: typeof MockSocketMessage.ON_RANKING_UPDATE.data) {
        this.ranking.setInfo(data);
    }
}

