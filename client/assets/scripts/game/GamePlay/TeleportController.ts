import { _decorator, Component, Node, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('TeleportController')
export class TeleportController extends Component {
    public playerEnter()
    {
        // console.log("PLAYER ENTER TELEPORT", this.node.position);
    }

    public playerExit()
    {
        // console.log("PLAYER EXIT TELEPORT", this.node.position);
    }
}

