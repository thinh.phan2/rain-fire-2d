import { GameEvent } from './../define';
import { TeleportManager } from './TeleportManager';
import { ItemManager } from './ItemManager';
import { MapCell } from './MapCell';
import { _decorator, Component, Node, Prefab, instantiate, Vec3 } from 'cc';
import { Screen } from '../../ui/screens/Screen';
import { ScreenName, SocketMessageTitle, UIGamePlayEvent } from '../define';
import { PlayerManager } from './PlayerManager';
import EventListener from '../../EventListener';
import { ScreenMgr } from '../../ui/screens/ScreenMgr';
import { Utils } from '../../Utils';
import { MockData } from '../../../data/MockData';
import { SocketManager } from '../../Network/SocketManager';


const { ccclass, property } = _decorator;

@ccclass('GamePlay')
export class GamePlay extends Screen {
    @property(PlayerManager) playerManager: PlayerManager = null;
    @property(ItemManager) itemManager: ItemManager = null;
    @property(TeleportManager) teleportManager: TeleportManager = null;
    @property(Prefab) mapCell: Prefab = null;
    @property(Node) map: Node = null;

    show() {
        super.show();
        SocketManager.instance.onMessage = this.onReceiveMessageFromServerToClient.bind(this);
    }

    onReceiveMessageFromServerToClient(message) {
        switch (message.title) {
            case SocketMessageTitle.InitMap:
                this.initMap(message.data)
                break;
            case SocketMessageTitle.PlayerMove:
                this.playerManager.movePlayer(message.data);
                break;
            case SocketMessageTitle.PlayerShot:
                this.playerManager.setBullet(message.data);
                break;
            case SocketMessageTitle.PlayerClaimItem:
                this.handlePlayclaimItem(message.data);
                break;
            case SocketMessageTitle.PlayerEnterTeleport:
                this.teleportManager.enterTeleport(message.data);
                break;
            case SocketMessageTitle.PlayerUpdateStatus:
                this.playerManager.updatePlayerStatus(message.data);
                break;
            case SocketMessageTitle.PlayerRespawn:
                this.playerManager.respawnPlayer(message.data);
                break;
            case SocketMessageTitle.ItemRespawn:
                this.itemManager.respawnItem(message.data);
                break;
            case SocketMessageTitle.RankingUpdate:
                EventListener.emit(UIGamePlayEvent.UpdateRanking, message.data);
                break;
            case SocketMessageTitle.EndGame:
                this.hide();
                ScreenMgr.instance.show(ScreenName.EndGame);
                EventListener.emit(GameEvent.UpdateEndGame, message.data);
                break;
        }
    }

    hide() {
        super.hide();
        this.resetGame();
        this.playerManager.setCamera(null);
    }

    initMap(data) {
        for (let i = 0; i < data.heigth; i++) {
            for (let j = 0; j < data.width; j++) {
                if (data.mapData[i][j].tile != 0) {
                    let cell = instantiate(this.mapCell);
                    cell.active = true;
                    cell.setParent(this.map);
                    cell.setPosition(new Vec3(j * MockData.unitMap, i * MockData.unitMap, 0));
                    cell.getComponent(MapCell).setSprite(data.mapData[i][j].tile - 1);
                }
            }
        }

        for (let [playerID, player] of data.players) {
            this.playerManager.initPlayer(player);
        }

        this.itemManager.spawnsItem(data.items);
        this.teleportManager.spawnsTeleport(data.teleports);
        
        EventListener.emit(UIGamePlayEvent.UpdateTimer, data.timer);
    }

    handlePlayclaimItem(data) {
        this.itemManager.destroyItem(data.itemID);
        this.playerManager.claimItem(data.playerID);
    }

    resetGame() {
        Utils.destroyAllChild(this.map);
        this.itemManager.reset();
        this.playerManager.reset();
        this.teleportManager.reset();
    }
}

