import { _decorator, Component, Node, Prefab, Color, instantiate, Sprite, Vec3 } from 'cc';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { TeleportController } from './TeleportController';
import { Utils } from '../../Utils';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('TeleportManager')
export class TeleportManager extends Component {
    @property(Prefab) teleportPrefab: Prefab = null;

    public spawnsTeleport(teleports) {
        for (let [teleportID, teleportData] of teleports) {
            let position = new Vec3(teleportData.position.y * MockData.unitMap, teleportData.position.x * MockData.unitMap, 0);
            let color = new Color(teleportData.color.x, teleportData.color.y, teleportData.color.z);
            this.initTeleport(teleportID, position, color);
        }
    }

    private initTeleport(teleportID, position: Vec3, color: Color) {
        let teleport = instantiate(this.teleportPrefab);
        teleport.active = true;
        teleport.setParent(this.node);
        teleport.setPosition(position)
        teleport.getComponent(Sprite).color = color;
        teleport.name = teleportID;
    }

    private findItem(id): Node {
        for (let i = 0; i < this.node.children.length; i++) {
            if (this.node.children[i].name === id) {
                return this.node.children[i];
            }
        }
        return null;
    }

    public enterTeleport(data: typeof MockSocketMessage.ON_PLAYER_ENTER_TELEPORT.data) {
        let teleport = this.findItem(data.teleportID);
        if (teleport) {
            teleport.getComponent(TeleportController).playerEnter();
        }

        setTimeout(() => {
            let destination = this.findItem(data.destinationID);
            if (destination) {
                destination.getComponent(TeleportController).playerExit();
            }
        }, data.timeTeleport)
    }

    public reset()
    {
        Utils.destroyAllChild(this.node);
    }
}

