import { MySprite } from './../../MySprite';
import { _decorator, Component, Node, Enum, SpriteFrame } from 'cc';
import { ItemType, Layer } from '../define';
const { ccclass, property } = _decorator;

Enum(ItemType)

@ccclass('ItemControllder')
export class ItemControllder extends Component {
    @property({type: ItemType}) type: ItemType = ItemType.Health;
    @property(MySprite) image: MySprite = null;
    @property(SpriteFrame) listImageItem: SpriteFrame[] = [];

    spawn(type: ItemType)
    {
        this.type = type;
        this.image.setSprite(this.listImageItem[type], false, true);
    }
}

