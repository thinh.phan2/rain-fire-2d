import { GameEvent, ServerDefine } from './../define';
import { _decorator, Component, Node, math, Vec3, Camera, UITransformComponent, Vec2, Tween, tween } from 'cc';
import EventListener from '../../EventListener';
const { ccclass, property } = _decorator;

@ccclass('CameraController')
export class CameraController extends Component {
    private _player: Node = null;
    private originPosition: Vec3 = null;
    private boundHeight: Vec2 = new Vec2();
    private boundWidth: Vec2 = new Vec2();

    public set Player(v) {
        this._player = v;
    }

    start() {
        this.originPosition = this.node.position.clone();
        let uiTransform = this.node.parent.getComponent(UITransformComponent);
        this.boundWidth.x = uiTransform.width / 2;
        this.boundWidth.y = 2800 - uiTransform.width / 2;
        this.boundHeight.x = uiTransform.height / 2;
        this.boundHeight.y = 1520 - uiTransform.height / 2;

        EventListener.on(GameEvent.UpdateCamera, this.updateCamera, this);
    }

    updateCamera() {
        if (this._player) {
            let targetPosition = this._player.position.clone();
            targetPosition.x = math.clamp(targetPosition.x, this.boundWidth.x - 20, this.boundWidth.y + 20);
            targetPosition.y = math.clamp(targetPosition.y, this.boundHeight.x - 20, this.boundHeight.y + 20);
            this.node.setPosition(targetPosition);
        } 
    }

    update(dt: number)
    {
        if (!this._player) {
            this.node.setPosition(this.originPosition);
        }
    }
}

