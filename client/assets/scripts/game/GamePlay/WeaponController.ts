import { BulletController } from './BulletController';
import { SocketManager } from './../../Network/SocketManager';
import { SocketMessageTitle } from './../define';
import { _decorator, Component, Node, Enum, instantiate, Vec3, Prefab, Sprite, SpriteFrame, find } from 'cc';
import EventListener from '../../EventListener';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { UIGamePlayEvent, WeaponType } from '../define';
import { MockData } from '../../../data/MockData';
import { PlayerController } from './PlayerController';
const { ccclass, property } = _decorator;

Enum(WeaponType);

@ccclass('WeaponController')
export class WeaponController extends Component {
    @property({ type: WeaponType }) gunType: WeaponType = WeaponType.Pistol;
    @property(Sprite) weaponSprite: Sprite = null;
    @property(SpriteFrame) listWeaponSprite: SpriteFrame[] = [];
    @property(SpriteFrame) defaultWeaponSprite: SpriteFrame = null;
    @property(Prefab) listBulletPrefab: Prefab[] = [];
    @property(Node) firePoint: Node = null;

    private player: Node = null;

    private bulletHolder: Node = null;

    onLoad() {
        this.bulletHolder = find('Canvas/Container/GamePlay/BulletHolder')
        this.player = this.node.parent.parent;
    }

    public onFire(playerID: string) {
        let data: typeof MockSocketMessage.SEND_PLAYER_SHOT.data = {
            playerID
        }

        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.PlayerShot,
            data
        })
    }

    public setBullet(data: typeof MockSocketMessage.ON_PLAYER_SHOT.data) {
        let bullet = this.findBullet(data.bulletID);

        if (bullet != null) {
            if (data.isDestroy) {
                bullet.getComponent(BulletController).destroyBullet();
            }
        }
        else {
            if (!data.isDestroy) {
                bullet = instantiate(this.listBulletPrefab[this.gunType]);
                bullet.active = false;
                bullet.name = data.bulletID;
                bullet.setParent(this.bulletHolder);
                bullet.getComponent(BulletController).setInfo(data);
            }
        }
    }

    public findBullet(id: string) {
        for (var i = 0; i < this.bulletHolder.children.length; i++) {
            if (this.bulletHolder.children[i].name === id) {
                return this.bulletHolder.children[i];
            }
        }
        return null;
    }

    public changeGun(type: WeaponType) {
        this.gunType = type;
        this.weaponSprite.spriteFrame = this.listWeaponSprite[type];

        if (this.player.getComponent(PlayerController).isMainPlayer) {
            EventListener.emit(UIGamePlayEvent.UpdateWeapon, this.weaponSprite.spriteFrame, type == WeaponType.Pistol);
        }
    }
}

