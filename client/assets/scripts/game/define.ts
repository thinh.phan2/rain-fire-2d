
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

export enum ScreenName {
    Lobby = "Lobby",
    Room = "Room",
    GamePlay = "GamePlay",
    EndGame = "EndGame"
}

export enum PopupName {
    CreateRoom = "CreateRoom",
    InviteRoom = "InviteRoom",
    KickRoom = "KickRoom"
}

export enum SoundName {
    Click = "button_tap",
    Claim_Item = "claim_item",
    Hit = "hit",
    Jump = "jump",
    Respawn = "respawn",
    Teleport = "teleport",
    Playing = "playing",
}

export enum DevicePerformance {
    LowEnd = "low-end",
    MideEnd = "mid-end",
    HighEnd = "high-end"
}

export enum GameEvent {
    ProfileUpdate = "game-profile-update",
    CaptureScreenShoot = "game-capture-screen-shoot",
    UpdateRoom = "update-room",
    UpdateLobbyRoom = "update-lobby-room",
    UpdateCamera = "update-camera",
    UpdateEndGame = "update-endgame"
}

export enum UIGamePlayEvent {
    UpdateHeart = 'update-heart',
    UpdateShield = 'update-shield',
    UpdateWeapon = 'update-weapon',
    UpdateTrap = 'update-trap',
    Die = 'die',
    Respawn = 'respawn',
    UpdateRanking = 'update-ranking',
    UpdateTimer = 'update-timer'
}

export enum UiEvent {

}

export enum SoundEvent {
    Load = "sound-load",
    OnOff = "sound-on-off",
    PlaySfx = "sound-play-sfx",
    PlayMusic = "sound-play-music",
    StopSfx = "sound-stop-sfx",
    Stop = "sound-stop"
}

export enum PopupEvent {
    Load = "popup-load",
    Show = "popup-show",
    Hide = "popup-hide",
    HideAll = "popup-hide-all"
}

export enum MaxAPIEvent {
    request = "maxapi-request"
}

export class Layer {
    static readonly DEFAULT: number = 1 << 0;
    static readonly PLAYER: number = 1 << 1;
    static readonly ITEM: number = 1 << 2;
    static readonly TELEPORT: number = 1 << 3;
    static readonly BULLET: number = 1 << 4;
}

export enum WeaponType {
    Pistol,
    MP5,
    AK47,
    FireGun,
    Rocket
}

export enum ItemType {
    Health,
    Shield,
    Speed,
    Jump,
    Trap,
    MP5,
    AK47,
    FireGun,
    Rocket
}

export enum PlayerUpdateType {
    Health,
    Shield,
    Speed,
    Jump,
    Trap,
    Gun,
    Die,
    Respawn
}

export enum SocketMessageTitle {
    PlayerConnect = "player-connect",
    JoinRoom = "join-room",
    QuickJoinRoom = "quick-join-room",
    CreateRoom = "create-room",
    UpdateRoom = "update-room",
    KickOutRoom = "kick-out-room",
    OutRoom = "out-room",
    StartGame = "start-game",
    InitMap = "init-map",
    PlayerMove = "player-move",
    PlayerShot = "player-shot",
    PlayerClaimItem = "player-claim-item",
    PlayerEnterTeleport = "player-enter-teleport",
    PlayerUpdateStatus = "player-update-status",
    PlayerRespawn = "player-respawn",
    RankingUpdate = "ranking-update",
    ItemRespawn = "item-respawn",
    EndGame = "end-game",
    UpdateLobbyRoom = "update-lobby-room",
    JoinAvaiableRoom = "Join-avaiable-room",
    PlayerChangeName = "player-change-name",
}

export class ServerDefine {
    public static readonly FPS = 15;
    public static readonly timePerFrame = 1 / this.FPS;

    public static readonly invincibleAfterTakeDameTime = 0.5 * 1000;
    public static readonly invincibleAfterRespawnTime = 3 * 1000;
}
