import { SocketManager } from './../../Network/SocketManager';
import { GameEvent, SocketMessageTitle } from './../define';
import { GameMgr } from './../GameMgr';
import { RoomChatGroup } from './RoomChatGroup';
import { RoomLobbyGroup } from './RoomLobbyGroup';
import { RoomPlayerGroup } from './RoomPlayerGroup';
import { _decorator, Component, Node, Label } from 'cc';
import { Screen } from '../../ui/screens/Screen';
import { ScreenMgr } from '../../ui/screens/ScreenMgr';
import { ScreenName } from '../define';
import { Profile } from '../Profile';
import EventListener from '../../EventListener';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('Room')
export class Room extends Screen {
    @property(RoomPlayerGroup) roomPlayerGroup: RoomPlayerGroup = null;
    @property(RoomLobbyGroup) roomLobbyGroup: RoomLobbyGroup = null;
    @property(RoomChatGroup) roomChatGroup: RoomChatGroup = null;
    @property(Label) roomID: Label = null;
    @property(Node) playButton: Node = null;

    public static instance: Room = null;

    public roomData: typeof MockData.roomInfoData = null;

    onLoad() {
        if (Room.instance == null)
            Room.instance = this;
    }

    show() {
        super.show();
        this.setInfo();
        EventListener.on(GameEvent.UpdateRoom, this.setInfo.bind(this));
    }

    hide() {
        super.hide();
    }
    
    onBackClick() {
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.OutRoom,
            data: {
                playerID: Profile.instance.userID
            }
        })
    }

    onPlayClick() {
        GameMgr.instance.startGame();
    }

    private setInfo() {
        this.roomData = GameMgr.instance.RoomData;
        this.roomID.string = 'ROOM ID: ' + this.roomData.roomID;
        this.roomPlayerGroup.setInfo(this.roomData.users);
        this.roomLobbyGroup.setInfo(MockData.roomLobbyData.users);

        if(this.roomData.hostPlayerID == Profile.instance.userID 
            // && this.roomData.users.length > 1
            ) {
            this.playButton.active = true;
        }else{
            this.playButton.active = false;
        }
    }

    public isMineHost() {
        return Profile.instance.userID == this.roomData.hostPlayerID;
    }

    public isHost(playerID: string) {
        return playerID == this.roomData.hostPlayerID;
    }
}

