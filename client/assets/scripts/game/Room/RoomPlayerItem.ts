import { SocketManager } from './../../Network/SocketManager';
import { GameMgr } from './../GameMgr';
import { _decorator, Component, Node, Label } from 'cc';
import { SocketMessageTitle } from '../define';
import { Room } from './Room';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('RoomPlayerItem')
export class RoomPlayerItem extends Component {
    @property(Label) nameLabel: Label = null;
    @property(Node) hostIcon: Node = null;
    @property(Node) readyIcon: Node = null;
    @property(Node) kickButton: Node = null;

    private data: typeof MockData.roomInfoData.users[0] = null;

    setInfo(data: typeof MockData.roomInfoData.users[0]) {
        this.data = data;
        this.nameLabel.string = data.name;
        let isHost = Room.instance.isHost(data.playerID);
        this.hostIcon.active = isHost;
        this.kickButton.active = !isHost && Room.instance.isMineHost();
        this.readyIcon.active = false;
    }

    onKickClick() {
        // GameMgr.instance.SocketClient.sendMessageToServer({
        //     title: SocketMessageTitle.KickOutRoom,
        //     data: {
        //         roomID: GameMgr.instance.RoomData.roomID,
        //         playerID: this.data.playerID,
        //     }
        // })

        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.KickOutRoom,
            data: {
                roomID: GameMgr.instance.RoomData.roomID,
                playerID: this.data.playerID,
            }
        })
    }
}

