import { _decorator, Component, Node, Prefab, instantiate, EditBox, ScrollView } from 'cc';
import { RoomChatItem } from './RoomChatItem';
const { ccclass, property } = _decorator;

@ccclass('RoomChatGroup')
export class RoomChatGroup extends Component {
    @property(Prefab) chatItem: Prefab = null;
    @property(ScrollView) chatScrollView: ScrollView = null;
    @property(EditBox) chatEditBox: EditBox = null;


    reset()
    {
        this.chatScrollView.content.removeAllChildren();
    }

    addItem(chatContent: string)
    {
        let item = instantiate(this.chatItem);
        item.active = true;
        this.chatScrollView.content.addChild(item);
        let roomChatItem = item.getComponent(RoomChatItem);
        roomChatItem.setInfo(chatContent);
    }

    onSendClick()
    {
        let chatContent = this.chatEditBox.string;
        if(chatContent !== '')
        {
            this.addItem(chatContent);
            this.chatEditBox.string = '';
            this.chatScrollView.scrollToBottom();
        }
    }
}

