import { _decorator, Component, Node, Label } from 'cc';
import { Profile } from '../Profile';
const { ccclass, property } = _decorator;

@ccclass('RoomChatItem')
export class RoomChatItem extends Component {
    @property(Label) chatContent: Label = null;

    setInfo(chat: string)
    {
        this.chatContent.string = Profile.instance.userName + ': ' + chat;
    }
}

