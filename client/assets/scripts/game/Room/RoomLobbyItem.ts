import { _decorator, Component, Node, Label } from 'cc';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('RoomLobbyItem')
export class RoomLobbyItem extends Component {
    @property(Label) nameLabel: Label = null;
    @property(Node) addButton: Node = null;
    @property(Node) addEdIcon: Node = null;

    setInfo(data: typeof MockData.roomLobbyData.users[0]) {
        this.nameLabel.string = data.name;
        this.addButton.active = true;
        this.addEdIcon.active = false;
    }

    onAddClick() {
        this.addButton.active = false;
        this.addEdIcon.active = true;
    }
}

