import { RoomLobbyItem } from './RoomLobbyItem';
import { _decorator, Component, Node, Prefab, NodePool, instantiate } from 'cc';
import { MockData } from '../../../data/MockData';
const { ccclass, property } = _decorator;

@ccclass('RoomLobbyGroup')
export class RoomLobbyGroup extends Component {
    @property(Prefab) roomLobbyItem: Prefab = null;
    @property(Node) content: Node = null;

    private pool: NodePool = new NodePool();

    onLoad()
    {
        for(let i = 0; i < 10; i++)
        {
            let cell = instantiate(this.roomLobbyItem);
            this.pool.put(cell);
        }
    }

    public setInfo(data: typeof MockData.roomLobbyData.users)
    {
        let currentItem = this.content.children.length;

        for (let i = 0; i < data.length; i++) {
            let lobbyItem: RoomLobbyItem;
            if (i < currentItem) {
                lobbyItem = this.content.children[i].getComponent(RoomLobbyItem)
            } else {
                lobbyItem = this.getOrCreate();
                lobbyItem.node.active = true;
                lobbyItem.node.parent = this.content;
                this.content.addChild(lobbyItem.node);
            }
            lobbyItem.setInfo(data[i]);
        }

        let remain = this.content.children.length - data.length;
        while (remain > 0) {
            this.return(this.content.children[this.content.children.length - 1]);
            remain--;
        }
    }

    private getOrCreate(): RoomLobbyItem {
        let cell: Node = null
        if (this.pool.size() > 0) {
            cell = this.pool.get();
        } else {
            cell = instantiate(this.roomLobbyItem)
        }
        
        return cell.getComponent(RoomLobbyItem);
    }

    private return(node: Node) {
        this.pool.put(node);
    }
}

