import { _decorator, Component, Node, Prefab, NodePool, instantiate } from 'cc';
import { MockData } from '../../../data/MockData';
import { RoomPlayerItem } from './RoomPlayerItem';
const { ccclass, property } = _decorator;

@ccclass('RoomPlayerGroup')
export class RoomPlayerGroup extends Component {
    @property(Prefab) playerItem: Prefab = null;

    private pool: NodePool = new NodePool();

    onLoad()
    {
        for(let i = 0; i < 10; i++)
        {
            let cell = instantiate(this.playerItem);
            this.pool.put(cell);
        }
    }

    public setInfo(data: typeof MockData.roomInfoData.users)
    {
        let currentPlayer = this.node.children.length;

        for (let i = 0; i < data.length; i++) {
            let playerInfo: RoomPlayerItem;
            if (i < currentPlayer) {
                playerInfo = this.node.children[i].getComponent(RoomPlayerItem)
            } else {
                playerInfo = this.getOrCreate();
                playerInfo.node.active = true;
                playerInfo.node.parent = this.node;
                this.node.addChild(playerInfo.node);
            }
            playerInfo.setInfo(data[i]);
        }

        let remain = this.node.children.length - data.length;
        while (remain > 0) {
            this.return(this.node.children[this.node.children.length - 1]);
            remain--;
        }
    }

    private getOrCreate(): RoomPlayerItem {
        let cell: Node = null
        if (this.pool.size() > 0) {
            cell = this.pool.get();
        } else {
            cell = instantiate(this.playerItem)
        }
        
        return cell.getComponent(RoomPlayerItem);
    }

    private return(node: Node) {
        this.pool.put(node);
    }
}

