import { _decorator, Component, Node, Prefab, NodePool, instantiate } from 'cc';
import { MockData } from '../../../data/MockData';
import { RoomInfoItem } from './RoomInfoItem';
const { ccclass, property } = _decorator;

@ccclass('LobbyRoomInfo')
export class LobbyRoomInfo extends Component {
    @property(Prefab) lobbyRoomItem: Prefab = null;

    private pool: NodePool = new NodePool();

    onLoad()
    {
        for(let i = 0; i < 12; i++)
        {
            let cell = instantiate(this.lobbyRoomItem);
            this.pool.put(cell);
        }
    }
    
    start() {

    }

    update(deltaTime: number) {
        
    }

    public setInfo(data: typeof MockData.lobbyRoomInfoData)
    {
        let currentRoom = this.node.children.length;

        for (let i = 0; i < data.length; i++) {
            let roomInfo: RoomInfoItem;
            if (i < currentRoom) {
                roomInfo = this.node.children[i].getComponent(RoomInfoItem)
            } else {
                roomInfo = this.getOrCreate();
                roomInfo.node.active = true;
                roomInfo.node.parent = this.node;
                this.node.addChild(roomInfo.node);
            }
            roomInfo.setInfo(data[i]);
        }

        let remain = this.node.children.length - data.length;
        while (remain > 0) {
            this.return(this.node.children[this.node.children.length - 1]);
            remain--;
        }
    }

    private getOrCreate(): RoomInfoItem {
        let cell: Node = null
        if (this.pool.size() > 0) {
            cell = this.pool.get();
        } else {
            cell = instantiate(this.lobbyRoomItem)
        }
        
        return cell.getComponent(RoomInfoItem);
    }

    private return(node: Node) {
        this.pool.put(node);
    }
}

