import { SocketManager } from './../../Network/SocketManager';
import { _decorator, Component, Node, Button, Label, Sprite } from 'cc';
import { MockData } from '../../../data/MockData';
import { ScreenMgr } from '../../ui/screens/ScreenMgr';
import { ScreenName, SocketMessageTitle } from './../define';
import { Profile } from '../Profile';
const { ccclass, property } = _decorator;

@ccclass('RoomInfoItem')
export class RoomInfoItem extends Component {
    @property(Label) amountLabel: Label = null;
    @property(Sprite) mapSprite: Sprite = null;
    @property(Node) full: Node = null;
    @property(Node) playing: Node = null;

    private roomData: typeof MockData.lobbyRoomInfoData[0] = null;

    joinRoomClick() {
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.JoinRoom,
            data: {
                roomID: this.roomData.roomID,
                playerID: Profile.instance.userID
            }
        })
    }

    public setInfo(data: typeof MockData.lobbyRoomInfoData[0]) {
        this.roomData = data;
        this.amountLabel.string = data.currentUserCounter + '/' + data.maxUser;

        this.playing.active = data.isPlaying;
        this.full.active = data.currentUserCounter >= data.maxUser;
    }
}

