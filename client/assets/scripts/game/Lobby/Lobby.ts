import { SocketManager } from './../../Network/SocketManager';
import { GameMgr } from './../GameMgr';
import { _decorator, Component, Node, Label, EditBox } from 'cc';
import { Screen } from '../../ui/screens/Screen';
import { GameEvent, PopupName, SocketMessageTitle } from '../define';
import { LobbyRoomInfo } from './LobbyRoomInfo';
import { PopupMgr } from '../../ui/Popup/PopupMgr';
import EventListener from '../../EventListener';
import { Profile } from '../Profile';

const { ccclass, property } = _decorator;

@ccclass('Lobby')
export class Lobby extends Screen {
    @property(LobbyRoomInfo) lobbyRoomInfo: LobbyRoomInfo = null;
    @property(Label) currentPageLabel: Label = null;
    @property(EditBox) editBoxFindRoom: EditBox = null;
    @property(EditBox) nameEditBox: EditBox = null;

    private pagingNumber = 12;
    private maxPageIndex = 0;
    private currentPage = 0;

    show() {
        super.show();
    }

    hide() {
        super.hide();
    }

    onLoad() {
        EventListener.on(GameEvent.UpdateLobbyRoom, this.setRoomInfo.bind(this));
    }

    private getRoomData(page: number) {
        let data = GameMgr.instance.LobbyRoomData;
        this.maxPageIndex = Math.floor(data.length / this.pagingNumber);
        let start = page * this.pagingNumber;
        return data.slice(start, start + this.pagingNumber);
    }

    private setRoomInfo() {
        let data = this.getRoomData(this.currentPage);
        this.lobbyRoomInfo.setInfo(data);
        this.currentPageLabel.string = (this.currentPage + 1).toString();
    }

    start() {
        this.nameEditBox.string = Profile.instance.userName;
        this.setRoomInfo();
    }

    onEditName()
    {
        Profile.instance.userName = this.nameEditBox.string;
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.PlayerChangeName,
            data: {
                playerID: Profile.instance.userID,
                name: Profile.instance.userName
            }
        })
    }

    onPlayClick() {
        GameMgr.instance.quickJoinRoom();
    }

    onCreateRoomClick() {
        PopupMgr.instance.show(PopupName.CreateRoom);
    }

    onLeftPageClick() {
        if (this.currentPage == 0) return;
        this.currentPage -= 1;
        this.setRoomInfo();
    }

    onRightPageClick() {
        if (this.currentPage == this.maxPageIndex) return;
        this.currentPage += 1;
        this.setRoomInfo();
    }

    onFindClick() {
        let roomID = this.editBoxFindRoom.string;
        let data = GameMgr.instance.LobbyRoomData;
        let findData = data.filter((room) => room.roomID == roomID);
        this.lobbyRoomInfo.setInfo(findData);
    }
}

