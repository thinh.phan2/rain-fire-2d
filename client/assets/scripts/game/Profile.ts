import { Utils } from './../Utils';

import { _decorator, game, Component, math } from 'cc';
import EventListener from '../EventListener';
import { DevicePerformance, GameEvent } from './define';

const { ccclass, property } = _decorator;

class ProfileData {
    // id: string = "0938314514";
    id:string = Utils.uuid;
    token: string = "0938314514";
    uid: string = "";
    name: string = Utils.getRandomName();
    attackCount: number = 0;
}

@ccclass('Profile')
export class Profile extends Component {

    private data: ProfileData = new ProfileData();
    public devicePerformance: DevicePerformance = DevicePerformance.LowEnd;
    public isHighPerformanceDevice: boolean;

    static instance: Profile = null;
    public iSOk: boolean = false;

    public get userID(): string {
        return this.data.id;
    }
    public set userID(value: string) {
        this.data.id = value;
    }
    public get uid(): string {
        return this.data.uid;
    }
    public set uid(value: string) {
        this.data.uid = value;
    }
    public get userToken(): string {
        return this.data.token;
    }
    public set userToken(value: string) {
        this.data.token = value;
    }

    public get userName(): string {
        return this.data.name;
    }
    public set userName(value: string) {
        this.data.name = value;
    }

    public get attackCount(): number {
        return this.data.attackCount;
    }
    public set attackCount(value: number) {
        this.data.attackCount = value;
    }

    onLoad()
    {
        Profile.instance = this;
    }

    start()
    {
        EventListener.on(GameEvent.ProfileUpdate, this.OnUpdateUserInfo, this);
    }

    public OnUpdateUserInfo(data: any) {
        if (data) {
            this.userID = data.userId;
            this.userToken = data.token;
        }

        console.log("[Profile] OnUpdateUserInfo:", data);
        this.iSOk = true;
    }

    public SetMaxFps() {
        if (this.devicePerformance == DevicePerformance.LowEnd) {
            game.frameRate = 30;
        }
        else if (this.devicePerformance == DevicePerformance.MideEnd) {
            game.frameRate = 45;
        }
        else {
            game.frameRate = 60;
        }
    }

    public SetNormalFps() {
        if (this.devicePerformance == DevicePerformance.LowEnd) {
            game.frameRate = 30;
        }
        else if (this.devicePerformance == DevicePerformance.MideEnd) {
            game.frameRate = 30;
        }
        else {
            game.frameRate = 45;
        }
    }
}
