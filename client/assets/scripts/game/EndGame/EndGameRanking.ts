import { _decorator, Component, Node, Prefab, NodePool, instantiate } from 'cc';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { EndGameRankingItem } from './EndGameRankingItem';
const { ccclass, property } = _decorator;

@ccclass('EndGameRanking')
export class EndGameRanking extends Component {
    @property(Prefab) rankingItem: Prefab = null;

    private pool: NodePool = new NodePool();

    onLoad()
    {
        for(let i = 0; i < 5; i++)
        {
            let cell = instantiate(this.rankingItem);
            this.pool.put(cell);
        }
    }

    public setInfo(data: typeof MockSocketMessage.ON_END_GAME.data.rankingPlayers)
    {
        let currentPlayer = this.node.children.length;

        for (let i = 0; i < data.length; i++) {
            let playerInfo: EndGameRankingItem;
            if (i < currentPlayer) {
                playerInfo = this.node.children[i].getComponent(EndGameRankingItem)
            } else {
                playerInfo = this.getOrCreate();
                playerInfo.node.active = true;
                playerInfo.node.parent = this.node;
                this.node.addChild(playerInfo.node);
            }
            playerInfo.setInfo(data[i], i);
        }

        let remain = this.node.children.length - data.length;
        while (remain > 0) {
            this.return(this.node.children[this.node.children.length - 1]);
            remain--;
        }
    }

    private getOrCreate(): EndGameRankingItem {
        let cell: Node = null
        if (this.pool.size() > 0) {
            cell = this.pool.get();
        } else {
            cell = instantiate(this.rankingItem)
        }
        
        return cell.getComponent(EndGameRankingItem);
    }

    private return(node: Node) {
        this.pool.put(node);
    }
}

