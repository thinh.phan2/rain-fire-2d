import { _decorator, Component, Node, Label, Sprite, SpriteFrame, Color } from 'cc';
import { MockSocketMessage } from '../../FakeSocket/MockSocketMessage';
import { Profile } from '../Profile';
const { ccclass, property } = _decorator;

@ccclass('EndGameRankingItem')
export class EndGameRankingItem extends Component {
    @property(Label) nameLabel: Label = null;
    @property(Label) rank: Label = null;
    @property(Label) score: Label = null;
    @property(Sprite) cupIcon: Sprite = null;
    @property(SpriteFrame) listCups: SpriteFrame[] = [];
    @property(Sprite) background: Sprite = null;

    private data: typeof MockSocketMessage.ON_END_GAME.data.rankingPlayers[0] = null;

    setInfo(data: typeof MockSocketMessage.ON_END_GAME.data.rankingPlayers[0], rank: number) {
        this.data = data;
        this.nameLabel.string = data.name;
        this.score.string = 'KILL: ' + data.kill.toString() + '  ' + 'DIE: ' + data.die.toString();

        if (rank <= 2) {
            this.rank.node.active = false;
            this.cupIcon.node.active = true;
            this.cupIcon.spriteFrame = this.listCups[rank];
        } else {
            this.rank.node.active = true;
            this.cupIcon.node.active = false;
            this.rank.string = (rank + 1).toString();

        }

        if (data.playerID === Profile.instance.userID) {
            this.background.color = Color.YELLOW;
        } else {
            this.background.color = Color.WHITE;
        }
    }

}

