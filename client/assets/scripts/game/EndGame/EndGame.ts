import { MockSocketMessage } from './../../FakeSocket/MockSocketMessage';
import { GameMgr } from './../GameMgr';
import { _decorator, Component, Node } from 'cc';
import { Screen } from '../../ui/screens/Screen';
import { ScreenMgr } from '../../ui/screens/ScreenMgr';
import { ScreenName, GameEvent, SocketMessageTitle } from './../define';
import EventListener from '../../EventListener';
import { EndGameRanking } from './EndGameRanking';
import { SocketManager } from '../../Network/SocketManager';
import { Profile } from '../Profile';
const { ccclass, property } = _decorator;

@ccclass('EndGame')
export class EndGame extends Screen {
    @property(EndGameRanking) ranking: EndGameRanking = null;

    show()
    {
        super.show();
        EventListener.on(GameEvent.UpdateEndGame, this.setInfo, this)
    }

    hide()
    {
        super.hide();
    }

    setInfo(data: typeof MockSocketMessage.ON_END_GAME.data)
    {
        this.ranking.setInfo(data.rankingPlayers);
    }

    onLobbyClick()
    {
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.OutRoom,
            data: {
                playerID: Profile.instance.userID
            }
        })
        
        GameMgr.instance.goToLobby();
    }

    onPlayAgainClick()
    {
        GameMgr.instance.goToRoom();
    }
}

