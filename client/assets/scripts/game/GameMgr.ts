import { _decorator, Component, director, Director, view, PhysicsSystem2D } from 'cc';
import EventListener from '../EventListener';
import { MaxApiUtils } from '../MaxApi/MaxApiUtils';
import { Api } from '../Network/Api';
import { TrimBase64 } from '../TrimBase64';
import { PopupMgr } from '../ui/Popup/PopupMgr';
import { ScreenMgr } from '../ui/screens/ScreenMgr';
import { GameEvent, PopupName, ScreenName, SocketMessageTitle, SoundEvent } from './define';
import { Profile } from './Profile';
import { MockData } from '../../data/MockData';
import { SocketManager } from '../Network/SocketManager';
const { ccclass, property } = _decorator;
enum State {
    Init,
    CheckToken,
    GameConfig,
    Lobby,
    Room,
    StartGame,
    Playing,
    NetworkError
}
@ccclass('GameMgr')
export class GameMgr extends Component {
    private state: State;
    static instance: GameMgr = null;
    private isCapture: boolean = false;
    private captureCallBack: Function = null;
    private time: number = 0;

    private traceScreenLoadId: string = "";
    private traceScreenInterractionId: string = "";

    private roomData: typeof MockData.roomInfoData;
    private lobbyRoomData: typeof MockData.lobbyRoomInfoData;

    public get RoomData() {
        return this.roomData;
    }

    public get LobbyRoomData() {
        return this.lobbyRoomData;
    }

    constructor() {
        super();

        MaxApiUtils.request("startTraceScreenLoad", "ScreenMain").then((res: any) => {
            this.traceScreenLoadId = res.traceId;
        });
        MaxApiUtils.request("startTraceScreenInteraction", "ScreenMain").then((res: any) => {
            this.traceScreenInterractionId = res.traceId;
        });
    }

    start() {
        GameMgr.instance = this;

        if (window.OnHideLoading) {
            window.OnHideLoading();

            MaxApiUtils.request("stopTrace", this.traceScreenLoadId);
        }

        director.on(Director.EVENT_AFTER_DRAW, this.canvasAfterDraw, this);

        this.SetState(State.Init);

        EventListener.on(GameEvent.CaptureScreenShoot, this.captureScreen, this);

        {//enable physics
            PhysicsSystem2D.instance.enable = true;

            // PhysicsSystem2D.instance.debugDrawFlags = EPhysics2DDrawFlags.Aabb |
            //     EPhysics2DDrawFlags.Pair |
            //     EPhysics2DDrawFlags.CenterOfMass |
            //     EPhysics2DDrawFlags.Joint |
            //     EPhysics2DDrawFlags.Shape;
        }
    }

    update(dt: number) {
        switch (this.state) {
            case State.Init:
                break;
            case State.CheckToken:
                this.time += dt;
                if (Profile.instance.iSOk) {
                    SocketManager.instance.connect();
                    this.SetState(State.GameConfig);
                }
                else if (this.time > 1) {
                    if (Api.IS_DEV) {
                        EventListener.emit(GameEvent.ProfileUpdate, null);
                    }
                    else {
                        this.SetState(State.NetworkError);
                    }
                }
                break;
            case State.GameConfig:
                if (SocketManager.instance.isReady)
                    this.SetState(State.Lobby);
                break;
        }
    }

    SetState(state: State) {
        console.log(`[GameMgr] state: ${State[this.state]} => ${State[state]}`);

        this.state = state;
        switch (state) {
            case State.Init:

                MaxApiUtils.request("getProfile").then((res: any) => {
                    EventListener.emit(GameEvent.ProfileUpdate, res);
                });

                //load res
                PopupMgr.instance.load();
                ScreenMgr.instance.load();

                EventListener.emit(SoundEvent.Load);

                this.SetState(State.CheckToken);
                break;
            case State.CheckToken:
                this.time = 0;
                break;
            case State.GameConfig:
                break;
            case State.Lobby:
                SocketManager.instance.onMessage = this.onReceiveMessageFromServerToClient.bind(this);
                SocketManager.instance.sendMessageToServer({
                    title: SocketMessageTitle.UpdateLobbyRoom,
                    data: {
                        playerID: Profile.instance.userID
                    }
                })

                if (!ScreenMgr.instance.isShow(ScreenName.Lobby)) {
                    ScreenMgr.instance.hideAll();
                    ScreenMgr.instance.show(ScreenName.Lobby);
                }
                break;
            case State.Room:
                SocketManager.instance.onMessage = this.onReceiveMessageFromServerToClient.bind(this);
                if (!ScreenMgr.instance.isShow(ScreenName.Room)) {
                    SocketManager.instance.onMessage = this.onReceiveMessageFromServerToClient.bind(this);
                    ScreenMgr.instance.hideAll();
                    ScreenMgr.instance.show(ScreenName.Room);
                }
                break;
            case State.StartGame:
                SocketManager.instance.sendMessageToServer({
                    title: SocketMessageTitle.StartGame,
                    data: {}
                });
                break;
            case State.Playing:
                ScreenMgr.instance.hideAll();
                ScreenMgr.instance.show(ScreenName.GamePlay);
                MaxApiUtils.request("stopTrace", this.traceScreenInterractionId);
                break;
        }
    }

    public goToLobby() {
        this.SetState(State.Lobby);
    }

    public goToRoom() {
        this.SetState(State.Room);
    }

    public startGame() {
        this.SetState(State.StartGame);
    }

    public createRoom() {
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.CreateRoom,
            data: {
                playerID: Profile.instance.userID,
                name: Profile.instance.userName
            }
        });
    }

    public quickJoinRoom() {
        SocketManager.instance.sendMessageToServer({
            title: SocketMessageTitle.JoinAvaiableRoom,
            data: {
                playerID: Profile.instance.userID,
                name: Profile.instance.userName
            }
        });
    }

    public onReceiveMessageFromServerToClient(message) {
        switch (message.title) {
            case SocketMessageTitle.CreateRoom:
                if (message.data.hostPlayerID == Profile.instance.userID) {
                    this.roomData = message.data;
                    this.SetState(State.Room)
                }
                break;
            case SocketMessageTitle.JoinRoom:
                this.roomData = message.data;
                this.SetState(State.Room)
                break;
            case SocketMessageTitle.UpdateRoom:
                this.roomData = message.data;
                EventListener.emit(GameEvent.UpdateRoom);
                break;
            case SocketMessageTitle.KickOutRoom:
                PopupMgr.instance.show(PopupName.KickRoom);
                this.SetState(State.Lobby);
                break;
            case SocketMessageTitle.OutRoom:
                this.SetState(State.Lobby);
                break;
            case SocketMessageTitle.StartGame:
                this.SetState(State.Playing)
                break;
            case SocketMessageTitle.UpdateLobbyRoom:
                this.lobbyRoomData = message.data;
                EventListener.emit(GameEvent.UpdateLobbyRoom);
                break;
        }
    }


    //////////////////////CAPTURE SCREEN////////////////////
    private canvasAfterDraw() {
        if (this.isCapture) {
            this.isCapture = false;
            let canvas = document.getElementById("GameCanvas") as HTMLCanvasElement;

            var resizedCanvas = document.createElement("canvas");
            var resizedContext = resizedCanvas.getContext("2d");

            let w = view.getVisibleSize().width;
            let h = view.getVisibleSize().height;

            resizedCanvas.width = w;
            resizedCanvas.height = h;

            resizedContext.drawImage(canvas, 0, 0, w, h, 0, 0, w, h);
            let c = TrimBase64.trimCanvas(resizedCanvas);
            let image = c.toDataURL('image/png');
            this.captureCallBack(image);
        }
    }

    public captureScreen(callback: Function) {
        this.captureCallBack = callback;
        this.isCapture = true;
    }
}
