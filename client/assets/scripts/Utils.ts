
import { _decorator, Component, Node, math, tween, Vec3, Prefab, resources, AudioClip, AssetManager, Quat, ParticleSystem, SpriteFrame, Size, Sprite, Vec2 } from 'cc';
const { ccclass, property } = _decorator;

interface HTTPRequestOptions {
    token: string
    responseType: XMLHttpRequestResponseType
}

@ccclass('Utils')
export class Utils {
    public static PlayParticle(paricle: ParticleSystem) {
        paricle.play();
        let a = paricle.getComponentsInChildren(ParticleSystem);
        a.forEach(element => {
            element.play();
        });
    }
    public static StopParticle(paricle: ParticleSystem) {
        paricle.stop();
        let a = paricle.getComponentsInChildren(ParticleSystem);
        a.forEach(element => {
            element.stop();
        });
    }
    public static toLocalString(num: number): string {
        num = Math.round(num);
        return num.toLocaleString().replace(/","/g, ".");
    }

    static formatScore(score: number): string {
        return score.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    static cloneObject<T>(data: any): T {
        const keys = Object.keys(data)
        let t = Object.create(null)
        for (let i = 0, size = keys.length; i < size; i++) {
            t[keys[i]] = data[keys[i]]
        }
        return t
    }

    static getScale(targetSprite: SpriteFrame, size: Size) {
        const rect = targetSprite.rect;
        let rateX = size.width / rect.width;
        let rateY = size.height / rect.height;
        let target = rateY
        if (rateX > rateY)
            target = rateX;
        return target;
    }

    public static CorrectSpriteFrameSize(sprite: Sprite, size: Size) {
        let scale = Utils.getScale(sprite.spriteFrame, size);
        sprite.node.setScale(new Vec3(scale, scale, scale));
    }

    static getImageScale(targetSprite: SpriteFrame, size: Size): Vec2 {
        const rect = targetSprite.rect;

        if (rect.width > rect.height) {
            return new Vec2(1, (rect.height * size.width / rect.width) / size.height);
        } else {
            return new Vec2((rect.width * size.height / rect.height) / size.width, 1);
        }
    }

    public static CorrectImageFrameSize(sprite: Sprite, size: Size) {
        let scale = Utils.getImageScale(sprite.spriteFrame, size);
        sprite.node.setScale(new Vec3(scale.x, scale.y, 0));
    }

    public static getUrlVer(url: string): string {
        // return url + "?v=" + Api.VERSION;
        return url;
    }

    public static getTime(d: Date): string {
        const pad = (s: number) => { return (s < 10) ? '0' + s : s; }
        return `${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`
    }
    static getDateTimeVN(d: Date): string {
        const pad = (s: number) => { return (s < 10) ? '0' + s : s; }
        return `${[pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')}`
    }
    public static get uuid(): string {
        var dt = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }
    public static destroyAllChild(parent: Node) {
        parent.children.forEach(child => {
            child.destroy();
        });
        parent.removeAllChildren();
    }
    public static getPrefabRes(path: string, callback: (asset: Prefab) => void) {
        resources.load(path, Prefab, (err, res) => {
            if (err) {
                if (callback) {
                    callback(null);
                }
                return;
            }

            if (callback) {
                callback(res);
            }
        });
    }

    public static getAudioRes(path: string, callback: (asset: AudioClip) => void) {
        resources.load(path, AudioClip, (err, res) => {
            if (err) {
                if (callback) {
                    callback(null);
                }
                return;
            }

            if (callback) {
                callback(res);
            }
        });
    }

    public static getPrefabBoundle(boundle: AssetManager.Bundle, path: string, callback: (asset: Prefab) => void) {
        boundle.load(path, Prefab, (err, res) => {
            if (err) {
                if (callback) {
                    callback(null);
                }
                return;
            }

            if (callback) {
                callback(res);
            }
        });
    }

    public static getAudioBoundle(boundle: AssetManager.Bundle, path: string, callback: (asset: AudioClip) => void) {
        boundle.load(path, AudioClip, (err, res) => {
            if (err) {
                if (callback) {
                    callback(null);
                }
                return;
            }

            if (callback) {
                callback(res);
            }
        });
    }

    static truncate(text: string, limitWord: number): string {
        let words = text.split(" ");
        if (words.length > limitWord)
            return words.splice(0, limitWord).join(" ") + "...";
        return text;
    }

    static getAngleFrom2Vec = (v1: Vec2, v2: Vec2): number => {
        let rad = Math.atan2(v2.y - v1.y, v2.x - v1.x);

        /** range from 0 to 2PI */
        if (rad < 0) {
            rad = 2 * Math.PI + rad;
        }
        // if (angle > Math.PI) {
        //   angle -= 2 * Math.PI;
        // }
        return (rad * 180) / Math.PI;
    };

    public static RotationAroundNode(self: Node, pos: Vec3, axis: Vec3, angle: number): Quat {
        let _quat = new Quat();
        let v1 = new Vec3();
        let v2 = new Vec3();
        let pos2: Vec3 = self.position;
        let rad = math.toRadian(angle);
        // Calculate the quaternion based on the axis of rotation and 
        // the arc of rotation
        Quat.fromAxisAngle(_quat, axis, rad);
        // Subtract, the vector between the target point and the camera point
        Vec3.subtract(v1, pos2, pos);
        // Rotate the vector dir according to the calculated quaternion, and then
        // calculate the rotated distance
        Vec3.transformQuat(v2, v1, _quat);
        self.position = Vec3.add(v2, pos, v2);
        // Rotate the quaternion around the specified axis in world space 
        // according to the axis and radian 
        Quat.rotateAround(_quat, self.rotation, axis, rad);
        return _quat;
    }

    public static convertMilisecondToTime(milisecond: number): string {
        let seconds = Math.floor(milisecond / 1000) % 60 ;
        let minutes = Math.floor((milisecond / (1000*60)) % 60);

        let result = `${minutes}m:${seconds}s`;
        return result;
    }

    public static getRandomName(): string {
        let randomName = [
            "Liam",
            "Noah",
            "Oliver",
            "Elijah",
            "James",
            "William",
            "Benjamin",
            "Lucas",
            "Henry",
            "Theodore",
            "Jack",
            "Levi",
            "Alexander",
            "Jackson",
            "Mateo",
            "Daniel",
            "Michael",
            "Mason",
            "Sebastian",
            "Ethan",
            "Logan",
            "Owen",
            "Samuel",
            "Jacob",
            "Asher",
            "Aiden",
            "John",
            "Joseph",
            "Wyatt",
            "David",
            "Leo",
            "Luke",
            "Julian",
            "Hudson"]

        let randomIndex = Math.floor(Math.random() * randomName.length);
        return randomName[randomIndex];
    }
}
