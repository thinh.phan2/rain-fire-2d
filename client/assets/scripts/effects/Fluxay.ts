
import { _decorator, Component, Node, Material, Sprite, tween, CCFloat, EffectAsset } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Fluxay')
export class Fluxay extends Component {
    @property(CCFloat) private speed: number = 1;
    material: Material = null;
    time: number = 0;
    start() {
        let sprite = this.node.getComponent(Sprite);
        this.material = sprite.getMaterial(0);
        
        tween({ time: 0 })
            .set({ time: 0 })
            .to(5 / this.speed, { time: 1 }, {
                onUpdate: (target: any, ratio) => {
                    this.material.setProperty('time', target.time)
                }, easing: 'linear'
            })
            .union()
            .repeatForever()
            .start()
    }
}
