import { Api } from "./Api";
import { ApiMock } from "./ApiMock";
import { NetworkMgr } from "./NetworkMgr";

export type DeepLinkData = {
    title: string,
    description: string,
    imageLink: string,
    domain: string,
    originalLink?: string,
    fallbackLink: string,
    fallbackLinkPc: string,
    appParams?: {}
}

export type DeepLinkRes = {
    response_info: {
        error_massage: string,
        error_code: number,
        event_tracking: string
    },
    item: {
        shortLink: string
    }
}

export class GameApi {

    // static AppVazV2() {
    //     if (window.realtimeSinceStartup) {
    //         let loadtime = (Date.now() - window.realtimeSinceStartup) / 1000;
    //         let body = [
    //             {
    //                 eventType: "DUR",
    //                 appVersion: Api.VERSION,
    //                 osPlatform: "web",
    //                 env: Api.IS_DEV ? "dev" : "prod",
    //                 flow: "home",
    //                 step: "load_game_resources",
    //                 api: Api.GAME_ID,//vn_momo_web_megaluckywheel_game
    //                 serviceName: Api.SERVICE_NAME,//megaday
    //                 value: loadtime
    //             }
    //         ];
    //         console.log(body);
    //         return NetworkMgr.postRequest(
    //             Api.HOST + Api.APPVARZ_V2,
    //             body,
    //             result => { });
    //     }
    // }

    static genDeepLink(data: DeepLinkData) {
        return new Promise((resolve, reject) => {
            NetworkMgr.postRequest(Api.HOST + Api.DEEP_LINK, data).then((res: any)=>{
                if (res && res.item && res.item.shortLink) {
                    resolve?.(res.item.shortLink);
                }
                else {
                    resolve?.(null);
                }
            });
        });
    }
}
