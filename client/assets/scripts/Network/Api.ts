export class Api {
    static readonly DEBUG_TOKEN: string = "0938314514";
    static readonly IS_DEV: boolean = true;
    static readonly VERSION: string = "0.0.1";
    static readonly DEV_HOST: string = "https://m.dev.mservice.io/";
    static readonly PROD_HOST: string = "https://m.mservice.io/";
    static HOST: string = Api.IS_DEV ? Api.DEV_HOST : Api.PROD_HOST;

    static readonly DEEP_LINK: string = `/deeplink-universal/v1/content-dynamic`;
}