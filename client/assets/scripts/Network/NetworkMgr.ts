import { _decorator, SpriteFrame, assetManager, ImageAsset, Texture2D } from 'cc';
import { Profile } from '../game/Profile';
const { ccclass, property } = _decorator;

@ccclass('NetworkMgr')
export class NetworkMgr {

    static getRequest(
        url: string
    ) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", `Bearer ${Profile.instance.userToken}`);
            xhr.responseType = 'json';
            xhr.timeout = 3000;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status >= 200 && xhr.status < 400) {
                        resolve(xhr.response);
                    }
                    else {
                        resolve(null);
                    }
                }
            };
            xhr.open("GET", url, true);
            xhr.send();
        });
    }

    static postRequest(
        url: string,
        data: Object
    ) {
        return new Promise((resolve, reject) => {

            let xhr = new XMLHttpRequest();
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", `Bearer ${Profile.instance.userToken}`);
            xhr.responseType = 'json';
            xhr.timeout = 3000;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status >= 200 && xhr.status < 400) {
                        resolve(xhr.response);
                    }
                    else {
                        resolve(null);
                    }
                }
            };
            xhr.open("POST", url, true);
            xhr.send(JSON.stringify(data));
        });
    }

    static geSpriteFrameAsync(imgUrl: string): Promise<SpriteFrame> {
        imgUrl = imgUrl.replace("img.mservice.io", "atc-edge03.mservice.com.vn");
        return new Promise((resolve, reject) => {
            assetManager.loadRemote<ImageAsset>(imgUrl, { cacheEnabled: true, maxRetryCount: 0 }, (err, imageAsset: ImageAsset) => {
                if (err) {
                    resolve(null);
                    console.warn(err);
                } else {

                    if (imageAsset) {
                        const spriteFrame = new SpriteFrame();
                        const texture = new Texture2D();
                        texture.image = imageAsset;
                        spriteFrame.texture = texture;

                        resolve(spriteFrame);
                    }
                    else {
                        resolve(null);
                    }
                }
            })
        })
    }

}
