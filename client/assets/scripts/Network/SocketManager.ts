import { _decorator, Component, Node, Vec2 } from 'cc';
import io from 'socket.io-client/dist/socket.io.js';
import { Profile } from '../game/Profile';
import { Socket } from 'socket.io-client';
import { SocketMessageTitle } from '../game/define';

const { ccclass, property } = _decorator;

enum SocketState {
    CLOSED,
    ERROR,
    CONNECTING,
    OPEN,
}

@ccclass('SocketManager')
export class SocketManager extends Component {
    public static instance: SocketManager = null;

    private socket: Socket = null;
    private state: SocketState = null;

    public onMessage: Function = null;

    public get isReady() {
        return this.socket && this.state === SocketState.OPEN;
    }

    onLoad() {
        SocketManager.instance = this;
    }

    public connect() {
        console.log("[SOCKET] ws send request connect");
        this.socket = io("http://10.40.116.29:3000");
        this.state = SocketState.CONNECTING;
        this.setSocketEvent();
    }

    private setSocketEvent() {
        this.socket.on("connect", () => {
            this.onOpen();
        });

        this.socket.on("connect_error", () => {
            this.onError();
        });

        this.socket.on("disconnect", () => {
            this.onClose();
        });

        this.socket.onAny((title, data) => {
            let message = {
                title,
                data
            }

            if (this.onMessage != null) {
                this.onMessage(message);
            }
        })
    }

    private onOpen() {
        console.log("[SOCKET] ws is connected! " + this.socket.id);

        let player = {
            playerID: Profile.instance.userID,
            name: Profile.instance.userName
        }

        let message = {
            title: SocketMessageTitle.PlayerConnect,
            data: player
        }

        this.sendMessageToServer(message);

        this.state = SocketState.OPEN;
    }

    private onClose() {
        console.log("[SOCKET] ws is closed!");
        this.state = SocketState.CLOSED;
    }

    private onError() {
        console.log("[SOCKET] ws is error!");
        this.state = SocketState.ERROR;
        // setTimeout(() => {
        //     this.socket.connect();
        // }, 1000);
    }

    public sendMessageToServer(message) {
        this.socket.emit(message.title, message.data);
    }
}

