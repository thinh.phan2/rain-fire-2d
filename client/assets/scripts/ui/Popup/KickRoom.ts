import { GameMgr } from './../../game/GameMgr';
import { _decorator, Component, Node } from 'cc';
import { Popup } from './Popup';
const { ccclass, property } = _decorator;

@ccclass('KickRoom')
export class KickRoom extends Popup {
    show()
    {
        super.show();
    }

    hide()
    {
        super.hide();
    }

    onCancelClick()
    {
        this.hide();
    }
}

