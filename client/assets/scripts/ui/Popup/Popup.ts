
import { _decorator, Component, Node, tween, Vec3, Button } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Popup')
export class Popup extends Component {
    @property(Button)private btnClose: Button = null;
    @property(Node)protected container: Node = null;

    public onClose: Function = null;

    start() {
        this.btnClose.node.on("click", this.OnCloseClick.bind(this));
    }

    protected OnCloseClick() {
        this.onClose?.();
        this.hide();
    }

    public show(autoHide: boolean = false) {
        this.node.active = true;
        this.container.setScale(Vec3.ZERO);
        tween(this.container)
            .to(0.5, { scale: new Vec3(1, 1, 1) }, { easing: "bounceInOut" })
            .start();

        if (autoHide) {
            setTimeout(() => {
                this.hide();
            }, 3000);
        }
    }

    public hide(shouldRemove: boolean = true) {
        this.node.active = false;
        if (shouldRemove) {
            this.node.removeFromParent();
            this.node.destroy();
        }
    }
}