import { Popup } from './Popup';
import { _decorator, Component, Node } from 'cc';
import { ScreenMgr } from '../screens/ScreenMgr';
import { ScreenName } from '../../game/define';
const { ccclass, property } = _decorator;

@ccclass('JoinRoom')
export class JoinRoom extends Popup {
    show()
    {
        super.show();
    }

    hide()
    {
        super.hide();
    }

    onJoinClick()
    {
        this.hide();
        ScreenMgr.instance.hideAll();
        ScreenMgr.instance.show(ScreenName.Room);
    }

    onCancelClick()
    {
        this.hide();
    }
}

