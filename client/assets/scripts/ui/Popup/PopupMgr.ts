
import { _decorator, Component, Prefab, instantiate } from 'cc';
import { PopupName } from '../../game/define';
import { Utils } from '../../Utils';
import { Popup } from './Popup';
const { ccclass, property } = _decorator;

@ccclass('PopupMgr')
export class PopupMgr extends Component {
    @property(Prefab)
    private prefabs: Prefab[] = [];
    static instance: PopupMgr = null;

    onLoad() {
        PopupMgr.instance = this;
    }

    public show(name: PopupName, onClose: Function = null): Popup {
        if (!this.node.getChildByName(name)) {
            let prefab = this.prefabs.find(prefab => prefab.data.name == name);
            if (prefab) {
                let popup = instantiate(prefab).getComponent(Popup);
                if (onClose) {
                    popup.onClose = onClose;
                }
                popup.node.setParent(this.node);
                popup.node.name = name;
                popup.show();
                return popup;
            }
        }
    }

    public hide(name: PopupName) {
        this.node.children.forEach(child => {
            if (child.name == name) {
                child.getComponent(Popup).hide();
            }
        });
    }

    public hideAll() {
        this.node.children.forEach(child => {
            if (child.active) {
                child.getComponent(Popup).hide();
            }
        });
    }

    private isContain(name: string): boolean {
        let result = false;
        this.prefabs.forEach(child => {
            if (child.data.name == name) {
                result = true;
                return;
            }
        });
        return result;
    }

    public load() {
        let array = Object.keys(PopupName);
        array.forEach(element => {
            if (!this.isContain(element)) {

                Utils.getPrefabRes("prefabs/popups/" + element, (prefab) => {
                    if (prefab) {
                        this.prefabs.push(prefab);
                        console.log("Popup loaded: " + element);
                    }
                    else {
                        console.error("Popup loade fail: " + element);
                    }
                });
            }
        });
    }
}