import { GameMgr } from './../../game/GameMgr';
import { _decorator, Component, Node } from 'cc';
import { Popup } from './Popup';
const { ccclass, property } = _decorator;

@ccclass('CreateRoom')
export class CreateRoom extends Popup {
    show()
    {
        super.show();
    }

    hide()
    {
        super.hide();
    }

    onCreateClick()
    {
        this.hide();
        GameMgr.instance.createRoom();
    }

    onCancelClick()
    {
        this.hide();
    }
}

