import { _decorator, Component, Node, instantiate, resources, Prefab } from 'cc';
import { ScreenName } from '../../game/define';
import { Screen } from './Screen';

const { ccclass, property } = _decorator;

@ccclass('ScreenMgr')
export class ScreenMgr extends Component {
    @property(Node) Container: Node = null;
    @property(Node) scenes: Node[] = [];
    static instance: ScreenMgr = null;

    onLoad()
    {
        ScreenMgr.instance = this;
    }

    start() {
        
    }

    public isShow(name: ScreenName) {
        let scene = this.scenes.find(scene => scene.name == name);
        return scene && scene.active;
    }

    public isLoaded(name: ScreenName) {
        let scene = this.scenes.find(scene => scene.name == name);
        return scene;
    }

    public show(name: ScreenName): Node {
        let scene = this.scenes.find(scene => scene.name == name);
        if (scene && !scene.active) {
            scene.getComponent(Screen).show();
        }
        return scene;
    }

    public hide(name: ScreenName) {
        let scene = this.scenes.find(scene => scene.name == name);
        if (scene) {
            scene.getComponent(Screen).hide();
        }
    }

    public hideAll() {
        this.scenes.forEach(scene => {
            if (scene.active) {
                scene.getComponent(Screen).hide();
            }
        });
    }

    public isContain(name: string): boolean {
        let result = false;
        this.scenes.forEach(scene => {
            if (scene.name == name) {
                result = true;
                return;
            }
        });
        return result;
    }

    public load() {
        let array = Object.keys(ScreenName);
        array.forEach(element => {
            if (!this.isContain(element)) {
                resources.load<Prefab>("prefabs/screens/" + element, (err, prefab) => {
                    if (prefab) {
                        let screen = instantiate(prefab);
                        screen.active = false;
                        screen.name = element;
                        screen.setParent(this.Container);
                        this.scenes.push(screen);
                        console.log("Screen loaded: " + element);
                    }
                    else {
                        console.error("Screen prefab notfound: " + element);
                    }
                });
            }
        });
    }
}