
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Screen')
export class Screen extends Component {

    show()
    {
        this.node.active = true;
    }
    hide()
    {
        this.node.active = false;
    }
}