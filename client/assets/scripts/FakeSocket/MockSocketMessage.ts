import { MockData } from "../../data/MockData";
import { ItemType, PlayerUpdateType, SocketMessageTitle, WeaponType } from "../game/define";

export class MockSocketMessage {

    static readonly ON_CREATE_ROOM = {
        title: SocketMessageTitle.CreateRoom,
        data: MockData.roomInfoData
    }
    
    static readonly ON_QUICK_JOIN_ROOM = {
        title: SocketMessageTitle.QuickJoinRoom,
        data: MockData.roomInfoData
    }

    static readonly ON_UPDATE_ROOM = {
        title: SocketMessageTitle.JoinRoom,
        data: MockData.roomInfoData
    }

    static readonly SEND_PLAYER_MOVE = {
        title: SocketMessageTitle.PlayerMove,
        data: {
            playerID: '123',
            direction: 1,
            isJump: false
        }
    }

    static readonly ON_PLAYER_MOVE = {
        title: SocketMessageTitle.PlayerMove,
        data: {
            playerID: '123',
            x: 0,
            y: 0,
            isTeleport: false
        }
    }

    static readonly SEND_PLAYER_SHOT = {
        title: SocketMessageTitle.PlayerShot,
        data: {
            playerID: '123'
        }
    }

    static readonly ON_PLAYER_SHOT = {
        title: SocketMessageTitle.PlayerShot,
        data: {
            bulletID: '123',
            playerID: '123',
            x: 0,
            y: 0,
            yDestination: 0,
            speed: 0,
            direction: 0,
            isDestroy: false
        }
    }

    static readonly ON_PLAYER_CLAIM_ITEM = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            itemID: '123',
            playerID: '123'
        }
    }

    static readonly ON_ITEM_RESPAWN = {
        title: SocketMessageTitle.ItemRespawn,
        data: {
            itemID: '123',
            type: ItemType.AK47
        }
    }


    static readonly ON_PLAYER_UPDATE_HEALTH = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            playerID: '123',
            updateType: PlayerUpdateType.Health,
            health: 0,
        }
    }

    static readonly ON_PLAYER_UPDATE_SHIELD = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            playerID: '123',
            updateType: PlayerUpdateType.Shield,
            shield: 0,
        }
    }

    static readonly ON_PLAYER_UPDATE_SPEED = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            playerID: '123',
            updateType: PlayerUpdateType.Speed,
            enable: false,
        }
    }

    static readonly ON_PLAYER_UPDATE_JUMP = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            playerID: '123',
            updateType: PlayerUpdateType.Jump,
            enable: false,
        }
    }

    static readonly ON_PLAYER_UPDATE_GUN = {
        title: SocketMessageTitle.PlayerUpdateStatus,
        data: {
            playerID: '123',
            updateType: PlayerUpdateType.Gun,
            type: WeaponType.Pistol
        }
    }

    static readonly ON_PLAYER_ENTER_TELEPORT = {
        title: SocketMessageTitle.PlayerEnterTeleport,
        data: {
            teleportID: '123',
            destinationID: '123',
            timeTeleport: 0
        }
    }

    static readonly ON_PLAYER_RESPAWN = {
        title: SocketMessageTitle.PlayerRespawn,
        data: {
            playerID: '123',
            x: 0,
            y: 0,
            health: 0,
            direction: 0
        }
    }

    static readonly ON_RANKING_UPDATE = {
        title: SocketMessageTitle.PlayerRespawn,
        data: [
            {
                playerID: '0938314514',
                name: 'THINH',
                kill: 0,
                die: 0
            },
            {
                playerID: '123',
                name: 'THINH 1',
                kill: 0,
                die: 0
            },
            {
                playerID: '123',
                name: 'THINH 2',
                kill: 0,
                die: 0
            },
            {
                playerID: '123',
                name: 'THINH 3',
                kill: 0,
                die: 0
            },
            {
                playerID: '123',
                name: 'THINH 6',
                kill: 0,
                die: 0
            },
        ]
    }

    static readonly ON_END_GAME = {
        title: SocketMessageTitle.EndGame,
        data: {
            rankingPlayers: [
                {
                    playerID: '123',
                    name: '123',
                    kill: 0,
                    die: 0
                },
                {
                    playerID: '123',
                    name: '123',
                    kill: 0,
                    die: 0
                }
            ]
        }
    }
}

