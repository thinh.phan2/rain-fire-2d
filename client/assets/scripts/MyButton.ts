import { _decorator, Component, tween, Vec3, Tween } from 'cc';
import EventListener from './EventListener';
import { SoundEvent, SoundName } from './game/define';
const { ccclass, property } = _decorator;

@ccclass('MyButton')
export class MyButton extends Component {

    tweenScale: Tween<Object> = null
    scale: Vec3 = null

    start() {
        this.scale = this.node.getScale();
        this.node.on('click', () => {
            let taget = new Vec3();
            Vec3.multiplyScalar(taget, this.scale, 0.5)
            this.node.setScale(taget);

            if(this.tweenScale)
            {
                this.tweenScale?.stop()
            }

            this.tweenScale = tween(this.node)
                .to(0.5, { scale: this.scale }, { easing: "elasticOut" })
                .start();

            EventListener.emit(SoundEvent.PlaySfx, SoundName.Click, true);
        });
    }
}
