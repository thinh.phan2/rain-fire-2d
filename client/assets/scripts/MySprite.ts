import { _decorator, Component, Node, Sprite, Texture2D, SpriteFrame, Vec3, assetManager, ImageAsset, Size, UITransform, resources } from 'cc';
import { Utils } from './Utils';
const { ccclass, property } = _decorator;

@ccclass('MySprite')
export class MySprite extends Component {
    private sprite: Sprite = null;
    private url: string = "";
    private size: Size;
    private isInit: boolean = false;

    init(): void {
        if (!this.isInit) {
            this.isInit = true;
            this.sprite = this.getComponent(Sprite);
            let uit = this.sprite.getComponent(UITransform);
            this.size = new Size(uit.width, uit.height);
            this.sprite.sizeMode = Sprite.SizeMode.CUSTOM;
        }
    }

    onLoad() {
        this.init();
    }

    setSpriteFrame(spriteFrame: SpriteFrame) {
        this.init();
        this.sprite.spriteFrame = spriteFrame;
    }

    setFixedSize(fixedSize: Size) {
        this.size = fixedSize.clone();
        this.sprite.sizeMode = Sprite.SizeMode.CUSTOM;
    }

    correctSpriteFrameSize(fixedSize: Size) {
        this.init();
        this.setFixedSize(fixedSize);
        Utils.CorrectSpriteFrameSize(this.sprite, this.size);
    }

    public Fetch(url: string) {
        this.init();
        if (url && this.url != url) {
            url = url.replace("img.mservice.io", "atc-edge03.mservice.com.vn");
            url = url.replace("img.mservice.com.vn", "atc-edge01.mservice.com.vn");
            assetManager.loadRemote<ImageAsset>(url, { cacheEnabled: true, maxRetryCount: 0 }, (err, imageAsset: ImageAsset) => {
                if (err) {
                    console.warn(err)
                } else {

                    if (imageAsset) {
                        let spriteFrame = new SpriteFrame();
                        let texture = new Texture2D();
                        texture.image = imageAsset;
                        spriteFrame.texture = texture;
                        this.setSprite(spriteFrame)
                    }
                }
            })
        }
    }

    public load(url: string) {
        resources.load<ImageAsset>(url, (err, imageAsset: ImageAsset) => {
            if (err) {
                console.warn(err)
            } else {

                if (imageAsset) {
                    let spriteFrame = new SpriteFrame();
                    let texture = new Texture2D();
                    texture.image = imageAsset;
                    spriteFrame.texture = texture;
                    this.setSprite(spriteFrame)
                }
            }
        });

    }

    public get Sprite(): Sprite {
        return this.sprite;
    }

    //isCorrectSize follow spriteFrame size
    public setSprite(spriteFrame: SpriteFrame, isResetScal: boolean = false, isImagetSize = false) {
        this.init();
        this.sprite.spriteFrame = spriteFrame;
        if (isResetScal) {
            this.node.scale = Vec3.ONE;
        } else if (isImagetSize) {
            Utils.CorrectImageFrameSize(this.sprite, this.size);
        }
        else {
            Utils.CorrectSpriteFrameSize(this.sprite, this.size);
        }
    }
}

