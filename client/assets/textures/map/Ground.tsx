<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.4" name="Ground" tilewidth="40" tileheight="40" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="40" height="40" source="DirtGreenGrass.png"/>
 </tile>
 <tile id="1">
  <image width="40" height="40" source="DirtRedGrass.png"/>
 </tile>
 <tile id="2">
  <image width="40" height="40" source="GreenGrass.png"/>
 </tile>
 <tile id="3">
  <image width="40" height="40" source="RedGrass.png"/>
 </tile>
</tileset>
