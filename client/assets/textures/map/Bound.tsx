<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.4" name="Bound" tilewidth="40" tileheight="40" tilecount="9" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="18">
  <image width="40" height="40" source="tile000.png"/>
 </tile>
 <tile id="19">
  <image width="40" height="40" source="tile001.png"/>
 </tile>
 <tile id="20">
  <image width="40" height="40" source="tile002.png"/>
 </tile>
 <tile id="21">
  <image width="40" height="40" source="tile004.png"/>
 </tile>
 <tile id="22">
  <image width="40" height="40" source="tile005.png"/>
 </tile>
 <tile id="23">
  <image width="40" height="40" source="tile006.png"/>
 </tile>
 <tile id="24">
  <image width="40" height="40" source="tile008.png"/>
 </tile>
 <tile id="25">
  <image width="40" height="40" source="tile009.png"/>
 </tile>
 <tile id="26">
  <image width="40" height="40" source="tile010.png"/>
 </tile>
</tileset>
