import { Utils } from "../scripts/Utils"

export class MockData {
    static readonly lobbyRoomInfoData = [
        {
            roomID: "1",
            currentUserCounter: 1,
            maxUser: 10,
            mapIndex: 1,
            isPlaying: false
        }
    ]

    static readonly roomInfoData = {
        roomID: '123456',
        hostPlayerID: '0938314514',
        maxUser: 10,
        users: [
            {
                playerID: '0938314514',
                name: 'THINH PHAN'
            },
            {
                playerID: '0938314515',
                name: 'THINH PHAN 1'
            }
        ]
    }

    static readonly roomLobbyData = {
        users: [
            {
                userID: '123456',
                name: 'THINH PHAN'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 1'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 2'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 3'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
            {
                userID: '123456',
                name: 'THINH PHAN 4'
            },
        ]
    }

    static readonly unitMap = 40;
}

